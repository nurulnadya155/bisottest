import {View, Text} from 'react-native';
import React from 'react';
import CustomButton from '../CustomButton';

const SocialSignInButtons = () => {
    const onSignInFacebook = () => {
      console.warn('onSignInFacebook');
    };
  
    const onSignInGoogle = () => {
      console.warn('onSignInGoogle');
    };
  
    
  return (
    <>
      <CustomButton
        text="Daftar dengan Facebook"
        onPress={onSignInFacebook}
        bgColor="#E7EAF4"
        fgColor="#4765A9"
      />

      <CustomButton
        text="Daftar dengan Google"
        onPress={onSignInGoogle}
        bgColor="#FAE9EA"
        fgColor="#DD4D44"
      />

    </>
  );
};

export default SocialSignInButtons;
