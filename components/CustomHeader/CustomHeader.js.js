import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import {Surface, Title} from 'react-native-paper';
import Feather from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';

const CustomHeader = ({
  menu,
  back,
  right,
  rightFunction,
  optionalIcon,
  optionalFunc,
  navigation,
  headerBg,
  iconColor,
}) => {
  return (
    
      <Surface style={[styles.header, {backgroundColor: headerBg}]}>
        <View style={styles.view}>
          {menu && (
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <Feather name="menu" size={24} color={iconColor}></Feather>
            </TouchableOpacity>
          )}
          {back && (
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Feather name="arrow-left" size={24} color={iconColor}></Feather>
            </TouchableOpacity>
          )}
        </View>
        <Image
          source={require('../../assets/images/Bisot_Logo.jpeg')}
          style={{textalign: 'center', width: 200, height: 50}}
        />
        <View style={[styles.view, styles.rightView]}>
          {optionalFunc && (
            <TouchableOpacity onPress={optionalFunc}>
              <Feather
                name={optionalIcon}
                size={28}
                color={iconColor}></Feather>
            </TouchableOpacity>
          )}
          {rightFunction && (
            <TouchableOpacity onPress={rightFunction}>
              <Feather name={right} size={28} color={iconColor}></Feather>
            </TouchableOpacity>
          )}
        </View>
      </Surface>
  );
};

const styles = StyleSheet.create({
  header: {
    height: 50,
    elevation: 4,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'black',
  },
  view: {
    flex: 1,
    margin: 10,
    alignItems: 'center',
    flexDirection: 'row',
  },
  rightView: {
    justifyContent: 'flex-end',
  },
});

export default CustomHeader;
