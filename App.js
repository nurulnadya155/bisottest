import React, { useContext, useEffect, useMemo } from 'react';
import {
  View, 
  ScrollView, 
  StyleSheet, 
  StatusBar
} from 'react-native';
import { 
  Provider as PaperProvider, 
  Divider, 
  List, 
  Text, 
  MD3LightTheme as DefaultTheme, 
  Badge 
} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import RNSecureStorage, { ACCESSIBLE } from 'rn-secure-storage';
import Snackbar from 'react-native-snackbar';

import ButtonNavigation from './components/CustomHeader';
import SignInScreen from './screens/SignInScreen/SignInScreen';
import HomeScreen from './screens/HomeScreen';
import SignUpScreen from './screens/SignUpScreen';
import StartPage from './screens/StartPage/StartPage';
import JawatanKosong from './screens/JawatanKosong';
import Acara from './screens/Acara';
import Perniagaan from './screens/Perniagaan';
import SenaraiEjen from './screens/SenaraiEjen';
import TentangKami from './screens/TentangKami';
import HubungiKami from './screens/HubungiKami';
import Pakej from './screens/Pakej';
import Produk from './screens/Produk';
import ViewProduk from './screens/Produk/ViewProduk';
import Dashboard from './screens/Dashboard';

import SenaraiProdukUntukEjen from './screens/Dashboard/SenaraiProdukUntukEjen';
// import ViewAcara from './screens/Acara/ViewAcara';

import { BASE_URL } from './hooks/Api';

import AuthContext from './context/Auth';
import ProdukTable from './screens/Dashboard/ProdukTable/ProdukTable';
import AcaraTable from './screens/Dashboard/AcaraTable/AcaraTable';
import JawatanKosongTable from './screens/Dashboard/JawatanKosongTable/JawatanKosongTable';
import PenambahanPakej from './screens/Dashboard/PenambahanPakej/PenambahanPakej';
import MaklumatProfil from './screens/Dashboard/Profil/MaklumatProfil';
import FormMesej from './screens/Dashboard/Mesej/FormMesej';
import FormProduk from './screens/Dashboard/FormProduk/FormProduk';
import FormAcara from './screens/Dashboard/FormAcara/FormAcara';
import FormJawatanKosong from './screens/Dashboard/FormJawatanKosong/FormJawatanKosong';
import Mesej from './screens/Dashboard/Mesej/Mesej';
import ViewMesej from './screens/Dashboard/Mesej/ViewMesej';
import ReplyFormMesej from './screens/Dashboard/Mesej/ReplyFormMesej';
import Ejen from './screens/SenaraiEjen/SenaraiEjen';
import TableSenaraiEjen from './screens/Agen/TableSenaraiEjen';
import AgenJualan from './screens/Agen/AgenJualan';

const theme = {
  ...DefaultTheme,
  "colors": {
    "primary": "rgb(52, 61, 255)",
    "onPrimary": "rgb(255, 255, 255)",
    "primaryContainer": "rgb(224, 224, 255)",
    "onPrimaryContainer": "rgb(0, 0, 110)",
    "secondary": "rgb(92, 93, 114)",
    "onSecondary": "rgb(255, 255, 255)",
    "secondaryContainer": "rgb(225, 224, 249)",
    "onSecondaryContainer": "rgb(25, 26, 44)",
    "tertiary": "rgb(120, 83, 107)",
    "onTertiary": "rgb(255, 255, 255)",
    "tertiaryContainer": "rgb(255, 216, 238)",
    "onTertiaryContainer": "rgb(46, 17, 38)",
    "error": "rgb(186, 26, 26)",
    "onError": "rgb(255, 255, 255)",
    "errorContainer": "rgb(255, 218, 214)",
    "onErrorContainer": "rgb(65, 0, 2)",
    "background": "rgb(255, 251, 255)",
    "onBackground": "rgb(27, 27, 31)",
    "surface": "rgb(255, 251, 255)",
    "onSurface": "rgb(27, 27, 31)",
    "surfaceVariant": "rgb(228, 225, 236)",
    "onSurfaceVariant": "rgb(70, 70, 79)",
    "outline": "rgb(119, 118, 128)",
    "outlineVariant": "rgb(199, 197, 208)",
    "shadow": "rgb(0, 0, 0)",
    "scrim": "rgb(0, 0, 0)",
    "inverseSurface": "rgb(48, 48, 52)",
    "inverseOnSurface": "rgb(243, 239, 244)",
    "inversePrimary": "rgb(190, 194, 255)",
    "elevation": {
      "level0": "transparent",
      "level1": "rgb(245, 242, 255)",
      "level2": "rgb(239, 236, 255)",
      "level3": "rgb(233, 230, 255)",
      "level4": "rgb(231, 228, 255)",
      "level5": "rgb(227, 224, 255)"
    },
    "surfaceDisabled": "rgba(27, 27, 31, 0.12)",
    "onSurfaceDisabled": "rgba(27, 27, 31, 0.38)",
    "backdrop": "rgba(48, 48, 56, 0.4)"
  }
};

const Drawer = createDrawerNavigator();
const Stack = createNativeStackNavigator();

function CustomNavigationBar({ navigation, back }) {
  return (
    <ButtonNavigation
      headerBg="#3ba2ff"
      iconColor="#000000"
      menu
      right="heart"
      rightFunction={() => console.log('right')}
      optionalIcon="bookmark"
      optionalFunc={() => console.log('optional')}
      navigation={navigation}
      back={back}
    />
  );
}

function CustomDrawerContent({ navigation }) {
  const { state, signOut } = useContext(AuthContext);

  useEffect(() => {
    // console.log(state.isSignout);
  }, [state]);

  return (
    <ScrollView>
      <List.Section>
        
        <List.Subheader>Akaun</List.Subheader>
        {typeof state.isSignout == 'undefined' || state.isSignout === true
          ? <>
              <List.Item 
                title="Log Masuk"
                left={props => <List.Icon {...props} icon="login-variant" />}
                onPress={() => navigation.navigate('AdminStack')}
              />
              <List.Item 
                title="Daftar"
                left={props => <List.Icon {...props} icon="account-plus-outline" />}
                onPress={() => navigation.navigate('SignUpScreen')}
              />
              <List.Item 
                title="Pakej"
                left={props => <List.Icon {...props} icon="package-variant-closed" />}
                onPress={() => navigation.navigate('Pakej')}
              />
            </>
          : <>
              <List.Item 
                title="Dashboard"
                left={props => <List.Icon {...props} icon="view-dashboard-outline" />}
                onPress={() => navigation.navigate('AdminStack', {screen: 'DashboardScreen',})}
              />
              <List.Item 
                title="Profil Saya"
                left={props => <List.Icon {...props} icon="account-outline" />}
                onPress={() => navigation.navigate('AdminStack', {screen: 'MaklumatProfilScreen',})}
              />
              <List.Item 
                title="Produk & Perkhidmatan"
                left={props => <List.Icon {...props} icon="package-variant-closed" />}
                onPress={() => navigation.navigate('AdminStack', {screen: 'ProdukTableScreen',})}
              />
              <List.Item 
                title="Acara"
                left={props => <List.Icon {...props} icon="calendar-outline" />}
                onPress={() => navigation.navigate('AdminStack', {screen: 'AcaraTableScreen',})}
              />
              <List.Item 
                title="Mesej"
                left={props => <List.Icon {...props} icon="message-outline" />}
                right={props => state && state.user && state.user.unread_messages_count ? <Badge>{state?.user?.unread_messages_count}</Badge> : null}
                onPress={() => navigation.navigate('AdminStack', {screen: 'MesejScreen',})}
              />
              <List.Item 
                title="Jawatan Kosong"
                left={props => <List.Icon {...props} icon="briefcase-variant-outline" />}
                onPress={() => navigation.navigate('AdminStack', {screen: 'JawatanKosongTableScreen',})}
              />
              <List.Item 
                title="Penambahan Pakej"
                left={props => <List.Icon {...props} icon="package-variant-closed" />}
                onPress={() => navigation.navigate('AdminStack', {screen: 'PenambahanPakejScreen',})}
              />
              <List.Item 
                title="Ejen Jualan"
                left={props => <List.Icon {...props} icon="badge-account-outline" />}
                onPress={() => navigation.navigate('AdminStack', {screen: 'EjenJualanScreen'})}
              />
              <List.Item 
                title="Senarai Ejen"
                left={props => <List.Icon {...props} icon="face-agent" />}
                onPress={() => navigation.navigate('AdminStack', {screen: 'SenaraiEjenScreen'})}
              />
              <List.Item 
                title="Log Keluar"
                left={props => <List.Icon {...props} icon="logout-variant" />}
                onPress={() => {
                  navigation.closeDrawer();
                  
                  Snackbar.show({
                    text: 'Anda ingin log keluar?',
                    duration: Snackbar.LENGTH_SHORT,
                    action: {
                      text: 'LOG KELUAR',
                      onPress: () => signOut(),
                    },
                  })
                }}
                // onLongPress={() => signOut()}
              />
            </>
          }

        <Divider />

        <List.Subheader>Menu Laman Web</List.Subheader>

        <List.Item 
          title="Laman Utama"
          left={props => <List.Icon {...props} icon="home-outline" />}
          onPress={() => navigation.navigate('HomeScreen')}
        />
        <List.Item 
          title="Senarai Ejen"
          left={props => <List.Icon {...props} icon="face-agent" />}
          onPress={() => navigation.navigate('SenaraiEjen')}
        />
        <List.Item 
          title="Jawatan Kosong"
          left={props => <List.Icon {...props} icon="briefcase-variant-outline" />}
          onPress={() => navigation.navigate('JawatanKosong')}
        />
        <List.Item 
          title="Acara"
          left={props => <List.Icon {...props} icon="calendar-outline" />}
          onPress={() => navigation.navigate('Acara')}
        />
        <List.Item 
          title="Perniagaan"
          left={props => <List.Icon {...props} icon="domain" />}
          onPress={() => navigation.navigate('Perniagaan')}
        />

        <Divider />

        <List.Subheader>Lain-Lain</List.Subheader>

        
        {/* <List.Item 
          title="Inbox"
          left={props => <List.Icon {...props} icon="inbox-outline" />}
          onPress={() => navigation.navigate('HomeScreen')}
        /> */}
        <List.Item 
          title="Tentang Kami"
          left={props => <List.Icon {...props} icon="information-outline" />}
          onPress={() => navigation.navigate('SenaraiProdukUntukEjen')}
        />
        <List.Item 
          title="Hubungi Kami"
          left={props => <List.Icon {...props} icon="chat-question-outline" />}
          onPress={() => navigation.navigate('HubungiKami')}
        />

      </List.Section>
    </ScrollView>
  )
}

const AdminStack = () => {
  const { state, signOut } = useContext(AuthContext);

  return (
    <Stack.Navigator>
      {typeof state.isSignout == 'undefined' || state.isSignout === true
        ? <Stack.Screen name="SignInScreen" component={SignInScreen} options={{ headerShown: false }} />
        : <>
            <Stack.Screen name="DashboardScreen" component={Dashboard} options={{ headerShown: false, title: 'Dashboard' }} />
            <Stack.Screen name="MaklumatProfilScreen" component={MaklumatProfil} options={{ headerShown: false, title: 'Profil' }} />
            <Stack.Screen name="ProdukTableScreen" component={ProdukTable} options={{ headerShown: false, title: 'Produk' }} />
            <Stack.Screen name="ProdukFormScreen" component={FormProduk} options={{ headerShown: true, title: 'Produk' }} />
            <Stack.Screen name="AcaraTableScreen" component={AcaraTable} options={{ headerShown: false, title: 'Acara' }} />
            <Stack.Screen name="AcaraFormScreen" component={FormAcara} options={{ headerShown: true, title: 'Acara' }} />
            <Stack.Screen name="MesejScreen" component={Mesej} options={{ headerShown: false, title: 'Mesej' }} />
            <Stack.Screen name="FormMesejScreen" component={FormMesej} options={{ headerShown: true, title: 'Mesej Baru' }} />
            <Stack.Screen name="ViewMesejScreen" component={ViewMesej} options={{ headerShown: true, title: 'Lihat Mesej' }} />
            <Stack.Screen name="ReplyMesejScreen" component={ReplyFormMesej} options={{ headerShown: true, title: 'Balas Mesej' }} />
            <Stack.Screen name="JawatanKosongTableScreen" component={JawatanKosongTable} options={{ headerShown: false, title: 'Jawatan Kosong' }} />
            <Stack.Screen name="JawatanKosongFormScreen" component={FormJawatanKosong} options={{ headerShown: true, title: 'Jawatan Kosong' }} />
            <Stack.Screen name="PenambahanPakejScreen" component={PenambahanPakej} options={{ headerShown: false, title: 'Penambahan Pakej' }} />
            <Stack.Screen name="EjenJualanScreen" component={AgenJualan} options={{ headerShown: false, title: 'Ejen Jualan' }} />
            <Stack.Screen name="SenaraiEjenScreen" component={TableSenaraiEjen} options={{ headerShown: false, title: 'Senarai Ejen' }} />
          </>
        }
    </Stack.Navigator>
  )
}

const App = () => {
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'LOADED':
          return {
            ...prevState,
            isLoading: false,
          };
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            user: action.user,
            userToken: action.token,
            isSignout: false,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            user: action.user,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      user: {},
    },
  );

  const authContext = React.useMemo(
    () => ({
      state: state,
      signIn: async (data) => {
        const response = await fetch(`${BASE_URL}/api/sanctum/token`, {
          method: 'POST',
          headers: {
            'Accept'      : 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
        });

        const json = await response.json();

        if (json && json.success) {
          const response = await fetch(`${BASE_URL}/api/user`, {
            // method: 'POST',
            headers: {
              'Accept'       : 'application/json',
              'Authorization': 'Bearer ' + json.token,
              'Content-Type' : 'application/json',
            },
          });

          const user = await response.json();

          RNSecureStorage.set('token', json.token, {accessible: ACCESSIBLE.WHEN_UNLOCKED})
            .then((res) => {
              console.log(res);
            }, (err) => {
              console.log(err);
            });

          dispatch({ type: 'SIGN_IN', token: json.token, user: user ? user : {} });

          Snackbar.show({
            text: 'Anda telah berjaya log masuk',
            duration: Snackbar.LENGTH_SHORT,
          });
        }

        return json;
      },
      signOut: () => {
        RNSecureStorage.remove('token').then((val) => {
            console.log(val)
          }).catch((err) => {
            console.log(err)
          });

        dispatch({ type: 'SIGN_OUT' });

        Snackbar.show({
          text: 'Anda telah log keluar',
          duration: Snackbar.LENGTH_SHORT,
        });
      },
      signUp: async (data) => {
        
      },
    })
  );

  useEffect(() => {
    const bootstrapAsync = async () => {
      RNSecureStorage.exists('token')
        .then((res) => {
          RNSecureStorage.get('token')
            .then(async (value) => {
              const response = await fetch(`${BASE_URL}/api/user`, {
                // method: 'POST',
                headers: {
                  'Accept'       : 'application/json',
                  'Authorization': 'Bearer ' + value,
                  'Content-Type' : 'application/json',
                },
              });
    
              const user = await response.json();

              dispatch({ type: 'RESTORE_TOKEN', token: value, user: user ? user : {} });
            })
            .catch((err) => {
              console.log(err)
            });
          });
    }

    bootstrapAsync();

    const timer = setTimeout(() => {
      dispatch({type: 'LOADED'});
    }, 3000);
    
    return () => clearTimeout(timer);
  }, []);

  return (
    <PaperProvider theme={theme}>
      <AuthContext.Provider value={authContext}>
        <NavigationContainer>
          <StatusBar backgroundColor={'#3ba2ff'} barStyle="light-content" />

        {state.isLoading
        ? <StartPage />
        : <>
          <Drawer.Navigator 
            initialRouteName="HomeScreen"
            screenOptions={{
              header: (props) => <CustomNavigationBar {...props} />,
            }}
            drawerContent={(props) => <CustomDrawerContent {...props} />}
          >
            <Drawer.Screen name="HomeScreen" component={HomeScreen} />
            <Drawer.Screen name="Pakej" component={Pakej} />
            <Drawer.Screen name="JawatanKosong" component={JawatanKosong} />
            <Drawer.Screen name="Acara" component={Acara} />
            <Drawer.Screen name="SenaraiEjen" component={SenaraiEjen} />
            <Drawer.Screen name="Perniagaan" component={Perniagaan} />
            <Drawer.Screen name="Produk" component={Produk} />
            <Drawer.Screen name="ViewProduk" component={ViewProduk} options={{ headerShown: true }} />
            <Drawer.Screen name="TentangKami" component={TentangKami} />
            <Drawer.Screen name="HubungiKami" component={HubungiKami} />
            <Drawer.Screen name="SignInScreen" component={SignInScreen} />
            <Drawer.Screen name="SignUpScreen" component={SignUpScreen} />
            <Drawer.Screen name="AdminStack" component={AdminStack} />
            <Drawer.Screen name="SenaraiProdukUntukEjen" component={SenaraiProdukUntukEjen} />
          </Drawer.Navigator>
        </>}
        </NavigationContainer>
      </AuthContext.Provider>
    </PaperProvider>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
});

export default App;
