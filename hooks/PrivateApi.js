import React, { useContext, useState, useEffect } from 'react';
import useSWR from 'swr';

import AuthContext from '../context/Auth';

import { BASE_URL } from './Api';

const fetcher = ({ uri, token }) => fetch(uri, { headers: { 'Authorization': 'Bearer ' + token }}).then(res => res.json())

function useEventIndex(query, status, pageIndex) {
    const { state } = useContext(AuthContext);

    if (status === undefined) {
        status = '';
    }

    if (pageIndex === undefined) {
        pageIndex = 0;
    }

    const { data, error, isLoading } = useSWR({uri: `${BASE_URL}/api/admin/events?q=${query}&status=${status}&page=${pageIndex}`, token: state.userToken}, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useEvent(eventId) {
    const { state } = useContext(AuthContext);

    if (eventId === undefined) {
        eventId = '';
    }

    const { data, error, isLoading } = useSWR({uri: `${BASE_URL}/api/admin/events/${eventId}`, token: state.userToken}, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useJobVacancyIndex(query, status, pageIndex) {
    const { state } = useContext(AuthContext);

    if (status === undefined) {
        status = '';
    }

    if (pageIndex === undefined) {
        pageIndex = 0;
    }

    const { data, error, isLoading } = useSWR({uri: `${BASE_URL}/api/admin/job-vacancies?q=${query}&status=${status}&page=${pageIndex}`, token: state.userToken}, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useJobVacancy(jobVacancyId) {
    const { state } = useContext(AuthContext);

    if (jobVacancyId === undefined) {
        jobVacancyId = '';
    }

    const { data, error, isLoading } = useSWR({uri: `${BASE_URL}/api/admin/job-vacancies/${jobVacancyId}`, token: state.userToken}, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useProductIndex(query, status, pageIndex) {
    const { state } = useContext(AuthContext);

    if (status === undefined) {
        status = '';
    }

    if (pageIndex === undefined) {
        pageIndex = 0;
    }

    const { data, error, isLoading } = useSWR({uri: `${BASE_URL}/api/admin/products?q=${query}&status=${status}&page=${pageIndex}`, token: state.userToken}, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useProduct(productId) {
    const { state } = useContext(AuthContext);

    if (productId === undefined) {
        productId = '';
    }

    const { data, error, isLoading } = useSWR({uri: `${BASE_URL}/api/admin/products/${productId}`, token: state.userToken}, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useMessageIndex(query, pageIndex) {
    const { state } = useContext(AuthContext);
    
    if (pageIndex === undefined) {
        pageIndex = 0;
    }

    const { data, error, isLoading } = useSWR({uri: `${BASE_URL}/api/admin/messages?page=${pageIndex}`, token: state.userToken}, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useMessage(messageId) {
    const { state } = useContext(AuthContext);

    const { data, error, isLoading } = useSWR({uri: `${BASE_URL}/api/admin/messages/${messageId}`, token: state.userToken}, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useUser() {
    const { state } = useContext(AuthContext);

    const { data, error, isLoading } = useSWR({uri: `${BASE_URL}/api/user`, token: state.userToken}, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useDashboardStats() {
    const { state } = useContext(AuthContext);

    const { data, error, isLoading } = useSWR({uri: `${BASE_URL}/api/admin/dashboard-stats`, token: state.userToken}, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

export {
    useEventIndex,
    useEvent,
    useJobVacancyIndex,
    useJobVacancy,
    useProductIndex,
    useProduct,
    useMessageIndex,
    useMessage,
    useUser,
    useDashboardStats,
};