import React, { useState, useEffect } from 'react';
import useSWR from 'swr';

const BASE_URL = 'https://staging.bisot.com.my';

const publicToken = '5SCUyT3WuAE2277phRCWGJoD6gUhaA';

const fetcher = (...args) => fetch(...args, { headers: { 'X-Access-Token': publicToken, 'X-Requested-With': 'XMLHttpRequest' }}).then(res => res.json())

function useGetIklanPremium() {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/premium-products`, fetcher);

    return {
        data: data && data.data ? data.data : [],
        isLoading,
        isError: error
    }
}

function useGetPublicCategories() {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/categories?status=Public`, fetcher);

    return {
        data: data && data.data ? data.data : [],
        isLoading,
        isError: error
    }
}

function useGetClosedCategories() {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/categories?status=Closed`, fetcher);

    return {
        data: data && data.data ? data.data : [],
        isLoading,
        isError: error
    }
}

function useGetPackages() {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/packages`, fetcher);

    return {
        data: data && data.data ? data.data : [],
        isLoading,
        isError: error
    }
}

function useGetJobVacancyCategories() {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/job-vacancies/categories`, fetcher);

    return {
        data: data && data.data ? data.data : [],
        isLoading,
        isError: error
    }
}

function useEventCategories() {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/events/categories`, fetcher);

    return {
        data: data && data.data ? data.data : [],
        isLoading,
        isError: error
    }
}

function useBusinessCategories() {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/business/categories`, fetcher);

    return {
        data: data && data.data ? data.data : [],
        isLoading,
        isError: error
    }
}

function useBusinessStates() {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/business/states`, fetcher);

    return {
        data: data && data.data ? data.data : [],
        isLoading,
        isError: error
    }
}

function useBusinessIndex(state, type, pageIndex) {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/business?state=${state}&type=${type}&page=${pageIndex}`, fetcher);

    if (state === undefined) {
        pageIndex = '';
    }

    if (type === undefined) {
        pageIndex = '';
    }

    if (pageIndex === undefined) {
        pageIndex = 0;
    }

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useEventIndex(type, pageIndex) {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/events?type=${type}&page=${pageIndex}`, fetcher);

    if (type === undefined) {
        type = '';
    }

    if (pageIndex === undefined) {
        pageIndex = 0;
    }

    console.log(`${BASE_URL}/api/events?type=${type}&page=${pageIndex}`);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useEvent(eventId) {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/events/${eventId}`, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useJobVacancyIndex(type, pageIndex) {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/job-vacancies?type=${type}&page=${pageIndex}`, fetcher);

    if (type === undefined) {
        pageIndex = '';
    }
    
    if (pageIndex === undefined) {
        pageIndex = 0;
    }

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useJobVacancy(jobVacancyId) {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/job-vacancies/${jobVacancyId}`, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useProductIndex(businessId, category, pageIndex) {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/products?business=${businessId}&category=${category}&page=${pageIndex}`, fetcher);

    if (businessId === undefined) {
        businessId = '';
    }

    if (category === undefined) {
        category = '';
    }
    
    if (pageIndex === undefined) {
        pageIndex = 0;
    }

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function useProduct(productId) {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/products/${productId}`, fetcher);

    return {
        data: data,
        isLoading,
        isError: error
    }
}

function usePackages() {
    const { data, error, isLoading } = useSWR(`${BASE_URL}/api/packages`, fetcher);

    return {
        data: data && data.data ? data.data : [],
        isLoading,
        isError: error
    }
}

function useLookups(type, parentId) {
    if (parentId === undefined) {
        parentId = '';
    }

    const { data, mutate, error, isLoading } = useSWR(`${BASE_URL}/api/lookups/${type}?parent=${parentId}&limit=100`, fetcher);

    return {
        data: data,
        mutate: mutate,
        isLoading,
        isError: error
    }
}

function useMalaysiaState(postcode) {
    if (postcode === undefined) {
        postcode = '';
    }

    const { data, mutate, error, isLoading } = useSWR(`${BASE_URL}/api/postcodes/state?postcode=${postcode}&limit=100`, fetcher);

    return {
        data: data,
        mutate: mutate,
        isLoading,
        isError: error
    }
}

function useCities(postcode) {
    if (postcode === undefined) {
        postcode = '';
    }

    const { data, mutate, error, isLoading } = useSWR(`${BASE_URL}/api/postcodes/cities?postcode=${postcode}&limit=100`, fetcher);

    return {
        data: data,
        mutate: mutate,
        isLoading,
        isError: error
    }
}

function useDistricts(postcode, state) {
    if (postcode === undefined) {
        postcode = '';
    }

    if (state === undefined) {
        state = '';
    }

    const { data, mutate, error, isLoading } = useSWR(`${BASE_URL}/api/postcodes/districts?postcode=${postcode}&state=${state}&&limit=100`, fetcher);

    return {
        data: data,
        mutate: mutate,
        isLoading,
        isError: error
    }
}

export {
    BASE_URL,
    useGetIklanPremium,
    useGetPublicCategories,
    useGetClosedCategories,
    useGetPackages,
    useGetJobVacancyCategories,
    useEventCategories,
    useBusinessCategories,
    useBusinessStates,
    useBusinessIndex,
    useEventIndex,
    useEvent,
    useJobVacancyIndex,
    useJobVacancy,
    usePackages,
    useProductIndex,
    useProduct,
    useLookups,
    useMalaysiaState,
    useCities,
    useDistricts,
};