import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import React from 'react';

import KategoriPekerjaan from './KategoriPekerjaan';
import SenaraiJawatanKosong from './SenaraiJawatanKosong';
import ViewJawatanKosong from './ViewJawatanKosong';
import IklanPremium from '../IklanPremium';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
const Stack = createNativeStackNavigator();

const Main = () => {
  return (
    <SafeAreaView>
        <Text style={styles.mainTxt}> Cari Pekerjaan Idaman Anda</Text>
        <IklanPremium />
        <KategoriPekerjaan />
      </SafeAreaView>
  )
}

const JawatanKosong = () => {
  return (
    <Stack.Navigator initialRouteName="Main">
      <Stack.Screen name="Main" component={Main} options={{ headerShown:false }} />
      <Stack.Screen name="SenaraiJawatanKosong" component={SenaraiJawatanKosong} options={{ title: 'Jawatan Kosong' }} />
      <Stack.Screen name="ViewJawatanKosong" component={ViewJawatanKosong} options={{ title: 'Jawatan Kosong' }} />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  mainTxt: {
    margin: 10,
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
  },
});

export default JawatanKosong;
