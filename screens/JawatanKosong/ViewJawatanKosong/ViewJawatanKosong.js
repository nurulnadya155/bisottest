import {View, SafeAreaView, Image, StyleSheet, ScrollView, RefreshControl} from 'react-native';
import React from 'react';
import {
  Divider,
  Text,
  Modal,
  Portal,
  Button,
  Provider,
  Card,
  Chip,
  List,
} from 'react-native-paper';

import Icon from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import ViewHubungiKami from '../HubungiJawatanKosong/HubungiJawatanKosong';

import { useJobVacancy } from '../../../hooks/Api';

const ViewJawatanKosong = ({ route, navigation }) => {
  // const [visible, setVisible] = React.useState(false);

  // const showModal = () => setVisible(true);
  // const hideModal = () => setVisible(false);
  // const containerStyle = {backgroundColor: 'white', padding: 20};

  const jobVacancyId = route?.params?.jobVacancyId;

  const { data, isLoading, isError } = useJobVacancy(jobVacancyId);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={isLoading} />
        }
      >
        <View style={{ padding: 20 }}>
          <Card style={{ marginBottom: 20 }}>
            <Card.Content>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 20 }}>
                <Text variant="bodyLarge">{data?.title}</Text>
                <Chip>RM{data?.min_salary}</Chip>
              </View>
              <List.Item
                title="Lokasi"
                right={(props) => <Text>&nbsp;</Text>}
              />
              <List.Item
                title="Kategori Pekerjaan"
                right={(props) => <Text>&nbsp;</Text>}
              />
              <List.Item
                title="Jenis Pekerjaan"
                right={(props) => <Text>&nbsp;</Text>}
              />
              <List.Item
                title="Gaji Min."
                right={(props) => <Text>RM{data?.min_salary}</Text>}
              />
              <List.Item
                title="Gaji Maks."
                right={(props) => <Text>RM{data?.max_salary}</Text>}
              />
              <List.Item
                title="Pengalaman Min."
                right={(props) => <Text>&nbsp;</Text>}
              />
              <List.Item
                title="Pengalaman Maks."
                right={(props) => <Text>&nbsp;</Text>}
              />
            </Card.Content>
          </Card>

          <Card>
            <Card.Title title="Maklumat Untuk Dihubungi" />
          </Card>
        </View>

        {/* <View>
          <Image
            source={require('../../../assets/images/kerani.jpg')}
            style={styles.image}
          />
        </View>
        <Text style={styles.textContainer}>Kerani</Text>
        <View style={{flexDirection: 'row', margin: 5}}>
          <Icon name="map-pin" />
          <Text style={{fontSize: 11, color: 'black', marginLeft: 5}}>
            {' '}
            Selangor
          </Text>
        </View>
        <View style={{flexDirection: 'row', margin: 5}}>
          <Icon name="calendar" />
          <Text style={{fontSize: 11, color: 'black', marginLeft: 5}}>
            {' '}
            23/01/2023
          </Text>
        </View>
        <View>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
            <View>
              <Text style={{width: 140, textAlign: 'center'}}>Penerangan</Text>
            </View>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.textPenerangan}>Dikirim oleh: </Text>
            <Text style={styles.textPenerangan}>Event ID: </Text>
          </View>

          <View
            style={{flexDirection: 'column', justifyContent: 'space-between'}}>
            <Text style={styles.textPenerangan}>Jenis Pekerjaan : </Text>
            <Text style={styles.textPenerangan}>Gaji Min : </Text>
            <Text style={styles.textPenerangan}>Gaji Maks : </Text>
            <Text style={styles.textPenerangan}>Pengalaman Maks : </Text>
          </View>
        </View>
        <View>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
            <View>
              <Text style={{width: 140, textAlign: 'center'}}>
                Dikirim Oleh
              </Text>
            </View>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          </View>
          <View style={{marginTop: 10, height: 250, width: null}}>
            <Image source={require('../../../assets/images/justebu.jpg')} />
          </View>
        </View>

        <View style={{marginTop: 10}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
            <View>
              <Text style={{width: 200, textAlign: 'center'}}>
                Maklumat Untuk Dihubungi
              </Text>
            </View>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          </View>
          <View style={styles.subView}>
            <View style={styles.container}>
              <Icon name="phone" size={15} color="#323232"></Icon>
              <Text style={styles.textPenerangan}> admin@bisot.com.my</Text>
            </View>

            <View style={styles.container}>
              <Icon name="whatsapp" size={15} color="#25D366"></Icon>
              <Text style={styles.textPenerangan}> admin@bisot.com.my</Text>
            </View>

            <View style={styles.container}>
              <Feather name="mail" size={15} color="red"></Feather>
              <Text style={styles.textPenerangan}> admin@bisot.com.my</Text>
            </View>
          </View>
          <View>
            <Portal>
              <Modal
                visible={visible}
                onDismiss={hideModal}
                contentContainerStyle={containerStyle}>
                    <ViewHubungiKami />
                </Modal>
            </Portal>
            <Button icon="message" textColor="#ea9999" onPress={showModal}>
              Hubungi Kami
            </Button>
          </View>
        </View> */}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  image: {
    flex: 0.5,
    width: null,
    height: 300,
    resizeMode: 'contain',
  },
  textContainer: {
    color: '#000',
    fontSize: 13,
    fontFamily: 'Sans-Serif',
    marginLeft: 10,
  },
  textPenerangan: {
    color: '#000',
    margin: 5,
    fontSize: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  icon: {
    margin: 5,
    backgroundColor: 'grey',
    borderRadius: 10,
  },
  container: {
    margin: 5,
    width: 150,
    alignItems: 'center',
    flexDirection: 'row',
  },
});

export default ViewJawatanKosong;
