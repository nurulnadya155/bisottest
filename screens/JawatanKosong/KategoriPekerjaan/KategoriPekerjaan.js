import {View, SafeAreaView, StyleSheet, TouchableOpacity} from 'react-native';
import { ActivityIndicator, Card, Text } from 'react-native-paper';
import React from 'react';
import { SvgUri } from 'react-native-svg';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL, useGetJobVacancyCategories } from '../../../hooks/Api';

const KategoriPekerjaan = () => {
  const navigation = useNavigation();

  const onViewJawatanKosongPressed = () => {

    navigation.navigate('ViewJawatanKosong');
  };

  const { data, isLoading, isError } = useGetJobVacancyCategories();

  if (isLoading) return <ActivityIndicator />;

  return (
    <SafeAreaView>
      <Text style={styles.mainTxt}>Kategori Pekerjaan</Text>
      <View>
        <View style={styles.subView}>
          {data && data.map(({ id, name, attributes, count, slug }) => (
            <View key={id} style={styles.container}>
              <Card
              // mode="contained"
              onPress={() => navigation.navigate('JawatanKosong', {
                screen: 'SenaraiJawatanKosong',
                params: {
                  type: slug,
                }
              })}
            >
              <Card.Content style={{ alignContent: 'center', alignItems: 'center', width: 90, height: 90 }}>
              <SvgUri
                  width="20"
                  height="20"
                  uri={'https://bisot.com.my/themes/pinlist/images/' + attributes['icon-pinlist']}
                  fill="blue"
                />
                <Text style={styles.txt}>
                  {name} ({count})
                </Text>
              </Card.Content>
            </Card>
          </View>
          ))}

          {/* {data && data.map(({ id, name, attributes, count, slug }) => (
            <TouchableOpacity 
              key={id}
              onPress={() => navigation.navigate('SenaraiJawatanKosong', {
                type: slug,
              })}
            >
              <View style={styles.container}>
                <SvgUri
                  width="20"
                  height="20"
                  uri={'https://bisot.com.my/themes/pinlist/images/' + attributes['icon-pinlist']}
                />
                <Text style={styles.txt}>{name} ({count})</Text>
              </View>
            </TouchableOpacity>
          ))} */}
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 5,
    // width: 88,
    // height: 80,
    alignItems: 'center',
    // paddingVertical: 8,
    // backgroundColor: '#e8ebf3',
  },
  subView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    margin: 5,
  },
  mainTxt: {
    margin: 5,
    marginTop: 30,
    fontSize: 15,
    color: '#0b5394',
  },
  txt: {
    // margin: 5,
    fontSize: 10,
    // color: '#231a14',
    textAlign: 'center',
  },
});

export default KategoriPekerjaan;
