import {
  View, 
  RefreshControl, 
  SafeAreaView, 
  StyleSheet, 
  ScrollView, 
  TouchableOpacity
} from 'react-native';
import { 
  ActivityIndicator, 
  Banner,
  Divider, 
  Text, 
  List 
} from 'react-native-paper';
import React, { useState } from 'react';
import { BASE_URL, useJobVacancyIndex } from '../../../hooks/Api';

const SenaraiJawatanKosong = ({ route, navigation }) => {
  const type = route.params && route.params.type ? route.params.type : '';

  const [pageIndex, setPageIndex] = useState(0);

  // const [refreshing, setRefreshing] = React.useState(false);

  const { data, isLoading, isError } = useJobVacancyIndex(type, pageIndex);

  const onRefresh = React.useCallback(() => {
    // setRefreshing(true);

    // setTimeout(() => {
    //   setRefreshing(false);
    // }, 2000);
  }, []);

  return (
    <SafeAreaView>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={onRefresh} />
        }
      >
        <Banner
          visible={!data || data && data.data && data.data.length == 0}
        >
            Tiada rekod
        </Banner>
        
        {/* <Text style={styles.mainTxt}>Jawatan Kosong</Text> */}
        {data && data.data && data.data.map(({ id, title, district, location, min_salary, applicant}) => (
          <TouchableOpacity 
            key={id}
            onPress={() => navigation.navigate('ViewJawatanKosong', {
              jobVacancyId: id,
            })}
          >
            <View key={id}>
              <Divider />
              <List.Item
                title={title}
                description={`${district}, ${location}`}
                left={props => (
                  <List.Image 
                    variant="image" 
                    source={{
                      uri: 
                        applicant && applicant.logo && applicant.logo.length 
                          ? `${BASE_URL}/${applicant.logo}` 
                          : 'https://ui-avatars.com/api/?name=' + (applicant && applicant.name && applicant.name.replace(' ', '+'))
                    }} 
                  />
                )}
                right={props => <Text variant="titleMedium">RM{min_salary}</Text>}
                style={{ paddingVertical: 0, paddingLeft: 10 }}
              />
              <Divider />
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainTxt: {
    margin: 10,
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
  },
});

export default SenaraiJawatanKosong;
