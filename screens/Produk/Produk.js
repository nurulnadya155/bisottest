import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import React from 'react';

import SenaraiProduk from './SenaraiProduk';
import ViewProduk from './ViewProduk';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
const Stack = createNativeStackNavigator();

const Produk = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="SenaraiProduk" 
        component={SenaraiProduk} 
        options={{ 
          title: 'Produk',
        }} 
      />
      <Stack.Screen name="ViewProduk" 
        component={ViewProduk} 
        options={{ 
          title: 'Produk',
        }} 
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  mainTxt: {
    margin: 10,
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
  },
});

export default Produk;
