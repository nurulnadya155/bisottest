import { View, SafeAreaView, Image, StyleSheet, ScrollView, RefreshControl} from 'react-native';
import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Card,
  Divider,
  Text,
  Modal,
  Portal,
  Button,
  Provider,
  Chip,
  List,
} from 'react-native-paper';
import { SliderBox } from "react-native-image-slider-box";

import Icon from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import MDIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import { BASE_URL, useProduct } from '../../../hooks/Api';

const ViewProduk = ({ route, navigation }) => {
  const produkId = route.params && route.params.produkId ? route.params.produkId : '';

  const { data, isLoading, isError } = useProduct(produkId);

  const [ images, setImages ] = useState([]);

  // const [visible, setVisible] = React.useState(false);

  // const showModal = () => setVisible(true);
  // const hideModal = () => setVisible(false);
  // const containerStyle = {backgroundColor: 'white', padding: 20};

  useEffect(() => {
    // setImages([]);

    // if (data && data.title) {
    //   navigation.setOptions({ title: data.title });
    // }

    var _images = [];

    if (data && data.images) {
      data.images.map((image) => {
        _images.push(BASE_URL + '/' + image.url);
      });

      setImages(_images);
    }
  }, [produkId, isLoading, data]);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={isLoading} />
        }
      >
        <SliderBox 
          autoplay={true}
          images={images}
          resizeMethod="resize"
          resizeMode="cover"
          ImageLoader={<ActivityIndicator />}
          LoaderComponent={props => <ActivityIndicator />}
        />

        <View style={{ padding: 20 }}>
          <Card>
            <Card.Content>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text variant="bodyLarge">{data?.title + '\nRM' + data?.price}</Text>
                <Chip>{data?.group_category?.name}</Chip>
              </View>
              <Text>{data?.description}</Text>
            </Card.Content>
          </Card>
        </View>

        <View style={{ padding: 20 }}>
          <Card>
            <Card.Content>
              <List.Item
                title={data?.applicant?.registration_type_id == 1 ? data?.applicant?.company_name : data?.applicant?.name}
                left={(props) => <List.Image variant="image" source={{uri: data?.applicant?.logo && data?.applicant?.logo.length ? `${BASE_URL}/${data?.applicant?.logo}` : 'https://ui-avatars.com/api/?name=' + data?.applicant?.name.replace(' ', '+')}} />}
              />
              <Divider />
              <List.Item
                title={data?.applicant?.address}
                titleNumberOfLines={3}
                description={[
                  data?.applicant?.postcode, 
                  data?.applicant?.city, 
                  data?.applicant?.district, 
                  data?.applicant?.state_code
                ].join(', ')}
                descriptionNumberOfLines={3}
                left={(props) => <MDIcon name="map-marker-outline" size={24} color="#000" />}
              />
              <Divider />
              <List.Item
                title={data?.applicant?.contact_person_telephone ? data?.applicant?.contact_person_telephone : data?.applicant?.telephone}
                description={data?.applicant?.contact_person ? data?.applicant?.contact_person : null}
                left={(props) => <MDIcon name="phone-outline" size={24} color="#000" />}
              />
              <Divider />
              <List.Item
                title={data?.applicant?.whatsapp_no}
                left={(props) => <MDIcon name="whatsapp" size={24} color="#000" />}
              />
              <Divider />
              <List.Item
                title={data?.applicant?.email}
                left={(props) => <MDIcon name="email-outline" size={24} color="#000" />}
              />

            </Card.Content>
          </Card>
        </View>

        <View style={{ padding: 20 }}>
          <Card>
            <Card.Content>
              <Text variant="bodyLarge">Senarai Ejen</Text>
            </Card.Content>
          </Card>
        </View>

        <View style={{ padding: 20 }}>
          <Card>
            <Card.Content>
              <Text variant="bodyLarge">Peta Lokasi</Text>
            </Card.Content>
          </Card>
        </View>

        {/* <View>
          <Image
            source={require('../../../assets/images/kerani.jpg')}
            style={styles.image}
          />
        </View>
        
        <View style={{flexDirection: 'row', margin: 5}}>
          <Icon name="calendar" />
          <Text style={{fontSize: 11, color: 'black', marginLeft: 5}}>
            {' '}
            Makanan & Minuman
          </Text>
        </View>
        <View style={{flexDirection: 'row', margin: 5}}>
          <Icon name="map-pin" />
          <Text style={{fontSize: 11, color: 'black', marginLeft: 5}}>
            {' '}
            Selangor
          </Text>
        </View>
        
        <View>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
            <View>
              <Text style={{width: 140, textAlign: 'center'}}>Penerangan</Text>
            </View>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.textPenerangan}>Dikirim oleh: </Text>
            <Text style={styles.textPenerangan}>Produk/Servis ID: </Text>
          </View>

          <View
            style={{flexDirection: 'column', justifyContent: 'space-between'}}>
            <Text style={styles.textPenerangan}>Laicikang Sodapp </Text>
            <Text style={styles.textPenerangan}>MOQ: 50 </Text>
            <Text style={styles.textPenerangan}>Harga Ejen: RM2.50 </Text>
            <Text style={styles.textPenerangan}>Jumlah Modal: RM125.00 </Text>
          </View>
        </View>

        <View>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
            <View>
              <Text style={{width: 140, textAlign: 'center'}}>Senarai Ejen</Text>
            </View>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          </View>

          <View
            style={{flexDirection: 'column', justifyContent: 'space-between'}}>
          </View>
        </View>


        <View>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
            <View>
              <Text style={{width: 140, textAlign: 'center'}}>
                Dikirim Oleh
              </Text>
            </View>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          </View>
          <View style={{marginTop: 10, height: 250, width: null}}>
            <Image source={require('../../../assets/images/justebu.jpg')} />
          </View>
        </View>

        <View style={{marginTop: 10}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
            <View>
              <Text style={{width: 200, textAlign: 'center'}}>
                Maklumat Untuk Dihubungi
              </Text>
            </View>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          </View>
            <View style={styles.container}>
              <Icon name="phone" size={15} color="#323232"></Icon>
              <Text style={styles.textPenerangan}> admin@bisot.com.my</Text>
            </View>

            <View style={styles.container}>
              <Icon name="whatsapp" size={15} color="#25D366"></Icon>
              <Text style={styles.textPenerangan}> admin@bisot.com.my</Text>
            </View>

            <View style={styles.container}>
              <Feather name="mail" size={15} color="red"></Feather>
              <Text style={styles.textPenerangan}> admin@bisot.com.my</Text>
            </View>
          <View>
            <Portal>
              <Modal
                visible={visible}
                onDismiss={hideModal}
                contentContainerStyle={containerStyle}>
                </Modal>
            </Portal>
            <Button icon="message" textColor="#ea9999" onPress={showModal}>
              Hubungi Kami
            </Button>
          </View>

          <View style={{marginTop: 15, marginBottom: 15}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
            <View>
              <Text style={{width: 150, textAlign: 'center'}}>Lokasi Peta</Text>
            </View>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          </View>
        </View>
        </View> */}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  image: {
    flex: 0.5,
    width: null,
    height: 300,
    resizeMode: 'contain',
  },
  textContainer: {
    color: '#000',
    fontSize: 15,
    fontFamily: 'Sans-Serif',
    margin: 10,
  },
  textPenerangan: {
    color: '#000',
    margin: 5,
    fontSize: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  icon: {
    margin: 5,
    backgroundColor: 'grey',
    borderRadius: 10,
  },
  container: {
    margin: 5,
    width: 150,
    alignItems: 'center',
    flexDirection: 'row',
  },
});

export default ViewProduk;
