import {
  View, 
  RefreshControl, 
  SafeAreaView, 
  StyleSheet, 
  ScrollView, 
  TouchableOpacity
} from 'react-native';
import { 
  ActivityIndicator, 
  Banner, 
  Divider, 
  Text, 
  List 
} from 'react-native-paper';
import React, { useState } from 'react';
import { BASE_URL, useProductIndex } from '../../../hooks/Api';

const SenaraiProduk = ({ route, navigation }) => {
  const businessId = route.params && route.params.businessId ? route.params.businessId : '';

  const category = route.params && route.params.category ? route.params.category : '';

  const [pageIndex, setPageIndex] = useState(0);

  // const [refreshing, setRefreshing] = React.useState(false);

  const { data, isLoading, isError } = useProductIndex(businessId, category, pageIndex);

  const onRefresh = React.useCallback(() => {
    // setRefreshing(true);

    // setTimeout(() => {
    //   setRefreshing(false);
    // }, 2000);
  }, []);

  // if (isLoading) return <ActivityIndicator />;
  
  return (
    <SafeAreaView>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={onRefresh} />
        }
      >
        {/* <Text style={styles.mainTxt}>Acara</Text> */}

        <Banner
          visible={!data || data && data.data && data.data.length == 0}
        >
            Tiada rekod
        </Banner>

        {data && data.data && data.data.map(({ id, applicant, product }) => (
          <TouchableOpacity 
            key={id}
            onPress={() => navigation.push('ViewProduk', {
              produkId: id,
            })}
          >
            <View key={id}>
              <Divider />
              <List.Item
                title={product.title}
                description={applicant && applicant.company_name.length ? applicant.company_name : applicant.name}
                left={props => <List.Image 
                    variant="image" 
                    source={{
                      uri: product 
                        && product.images 
                        && product.images[0] 
                        && product.images[0].url 
                        ? `${BASE_URL}/${product.images[0].url}` 
                        : 'https://ui-avatars.com/api/?name=' + product.title.replace(' ', '+')
                    }}
                  />
                }
                right={props => <Text variant="titleMedium">RM {product.price}</Text>}
                style={{ paddingVertical: 0, paddingLeft: 10 }}
              />
              <Divider />
            </View>
          </TouchableOpacity>
        ))}

      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainTxt: {
    margin: 10,
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
  },
});

export default SenaraiProduk;
