import { View, Text,SafeAreaView, StyleSheet, ScrollView } from 'react-native'
import React from 'react'

import {Dimensions} from 'react-native';

const window = Dimensions.get("window");

const Ejen = () => {
  return (
    <SafeAreaView>
      <Text style={styles.txt}> Senarai Ejen Berdaftar Untuk Meluaskan Perniagaan Anda</Text>
      <ScrollView horizontal={false} showsHorizontalScrollIndicator={false}>
        <View style={styles.View}>
            <View style={styles.container}>
              <Text style={{fontSize: 14}}>Kategori ini hanya untuk Usahawan Berdaftar Sahaja</Text>
            </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
    borderColor: '#dcdcdc',
    height: 30,
    alignContent: 'center'
  },
  View:{
    marginTop: 100,
    height: 180,
    alignContent: 'center',
    alignSelf: 'center'
  },
  txt: {
    fontSize: 18,
    marginTop: 10,
    color: '#000',
    textAlign: 'center',
  },
  subcontainer:{
    margin: 8,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#dcdcdc',
    width: 370,
    height: 100,
    backgroundColor: '#ffffff',
  }
});

export default Ejen