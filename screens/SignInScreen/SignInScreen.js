import React, { useEffect } from 'react';
import {
  View,
  // Text,
  StyleSheet,
  Image,
  StatusBar,
  // TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { ActivityIndicator, Button, Text, TextInput } from "react-native-paper";
import SocialSignInButtons from '../../components/SocialSignInButton';
import { useNavigation } from '@react-navigation/core';
import Spinner from 'react-native-loading-spinner-overlay';
import Snackbar from 'react-native-snackbar';
import { Formik } from 'formik';
import { useRef } from "react";

import AuthContext from '../../context/Auth';

const SignInScreen = () => {
  const { state, signIn } = React.useContext(AuthContext);

  const [deviceName, setDeviceName] = React.useState('');

  const navigation = useNavigation();

  const ref_to_password_input = useRef();

  // const onSignInPressed = () => {

  //   navigation.navigate('HomeScreen');
  // };

  const onSignUpPressed = () => {

    navigation.navigate('SignUpScreen');
  };

  useEffect(() => {
    DeviceInfo.getDeviceName().then((deviceName) => {
      setDeviceName(deviceName);
    });
  }, []);

  return (
    <KeyboardAvoidingView style={styles.container}>
      <StatusBar hidden={false} />
      <View style={styles.formBottomContainer}>
      <Text style={{fontSize: 20, color:'black'}}> Log Masuk</Text>
        <View style={styles.formBottomSubContainer}>
          <Formik
            enableReinitialize={true}
            initialValues={{ 
              device_name: deviceName,
              email: '', 
              password: '',
            }}
            onSubmit={async (values) => {
              if (values.email.length == 0 || values.password.length == 0) {
                Snackbar.show({
                  text: 'Sila masukkan alamat e-mel dan kata laluan',
                  duration: Snackbar.LENGTH_SHORT,
                });

                return;
              }
              
              const response = await signIn(values);

              setTimeout(() => {
                if (response && response.errors && response.errors.email[0]) {
                  console.log(response.errors.email[0]);
                  Snackbar.show({
                    text: response.errors.email[0],
                    duration: Snackbar.LENGTH_INDEFINITE,
                  });
                }
              }, 200);
            }
          }
          >
            {({ handleChange, handleBlur, handleSubmit, setFieldValue, isSubmitting, values }) => (
              <View>
                <Spinner
                  customIndicator={<ActivityIndicator size="large" />}
                  visible={isSubmitting}
                  textContent={'Log Masuk...'}
                  cancelable={true}
                />
          {/*  */}
          {/* <Text style={{color: '#000',  fontSize: 13}}>E-mel</Text> */}
          {/* <View style={styles.customInputContainer}> */}
          
          <TextInput
            label="E-mel"
            onChangeText={handleChange('email')}
            onBlur={handleBlur('email')}
            value={values.email}
            mode="outlined"
            style={styles.txtInput}
            keyboardType="email-address"
            returnKeyType="next"
            onSubmitEditing={() => ref_to_password_input.current.focus()}
          />
          {/* </View> */}
          {/*  */}
          {/*  */}
          {/* <Text style={{color: '#000', fontSize: 13}}>Kata Laluan</Text> */}
          {/* <View style={styles.customInputContainer}> */}
          <TextInput
            label="Kata Laluan"
            onChangeText={handleChange('password')}
            onBlur={handleBlur('password')}
            value={values.password}
            secureTextEntry={true}
            mode="outlined"
            style={styles.txtInput}
            ref={ref_to_password_input}
            returnKeyType="done"
            onSubmitEditing={handleSubmit}
          />
          {/* </View> */}
          {/*  */}
          {/*  */}
          <Button 
            onPress={handleSubmit} 
            mode="contained"
            style={{ marginTop: 30 }}
          >
            LOG MASUK
          </Button>
          {/* <TouchableOpacity 
          style={styles.loginButton}>
            <Text 
            onPress={onSignInPressed}
            style={{color: '#fff', fontWeight: 'bold', fontSize: 15}} >
              Daftar Masuk
            </Text>
          </TouchableOpacity> */}
          {/*  */}
          {/*  */}
          {/* <Text style={{textAlign: 'center', color: '#000'}}>Atau</Text> */}
          {/*  */}
          {/*  */}
          {/* <SocialSignInButtons /> */}
          {/*  */}
          {/*  */}
                </View>
              )}
            </Formik>
          <View>
            <View style={{flexDirection: 'row', marginVertical: 10}}>
              <Text style={{color: '#000'}}>Belum mempunyai akaun?</Text>
              <TouchableOpacity>
                <Text
                onPress={onSignUpPressed}
                  style={{
                    marginLeft: 5,
                    color: '#2B9BFF',
                    fontWeight: 'bold',
                  }}>
                  Daftar
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity>
              <Text
                style={{
                  marginLeft: 5,
                  color: '#2B9BFF',
                  fontWeight: 'bold',
                }}>
                Lupa Kata Laluan?
              </Text>
            </TouchableOpacity>
          </View>
          {/*  */}
        </View>
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  bottomBackgroundImgContainer: {
    flex: 1,
  },
  formContainer: {
    position: 'absolute',
    height: '100%',
    width: '100%',
  },
  formBottomContainer: {
    flex: 0.8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  formBottomSubContainer: {
    width: '95%',
    borderRadius: 10,
    backgroundColor: 'rgba(255,255,255,0.5)',
    padding: 20,
  },
  customInputContainer: {
    marginVertical: 10,
    borderWidth: 1,
    borderColor: '#2B9BFF',
    backgroundColor: '#fff',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 10,
    height: '12%',
  },
  loginButton: {
    backgroundColor: '#2B9BFF',
    padding: 10,
    alignItems: 'center',
    marginVertical: 10,
    borderRadius: 10,
  },
  txtInput: {
    marginBottom: 5,
  }
});

export default SignInScreen;
