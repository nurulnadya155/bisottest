import {
  View,
  StyleSheet,
  ScrollView,
  Image,
  Alert,
  Button,
  TouchableOpacity
} from 'react-native';
import { 
  ActivityIndicator, 
  Card,
  Text 
} from 'react-native-paper';
import React, { useEffect } from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';

import {BASE_URL, useGetIklanPremium } from '../../hooks/Api';

const iklanPremium = () => {
  const navigation = useNavigation();

  const { data, isLoading, isError } = useGetIklanPremium();

  if (isLoading) return <ActivityIndicator />;

  return (
    <SafeAreaView>
      <Text style={styles.txt}> Pakej Iklan Premium</Text>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={true}>
        <View>
          <View style={styles.subView}>
            {data && data.map(({ id, product }) => (
              <Card 
                key={id}
                onPress={() => {
                  navigation.navigate('ViewProduk', {
                    produkId: id,
                  })
                }}
                style={{ margin: 10, width: 200 }}
              >
                <Card.Cover
                  source={{
                    uri: `${BASE_URL}/${product.images[0].url}`
                  }}
                />
                <Card.Content>
                  <Text style={{ textAlign: 'center', padding: 0, paddingTop: 10 }}>
                    {product.title}
                  </Text>
                </Card.Content>
              </Card>
            ))}

            {/* {data && data.map(({ id, product }) => (
              <TouchableOpacity 
                key={id}
                onPress={() => {
                  navigation.navigate('Produk', {
                    screen: 'ViewProduk',
                    params: {
                      
                    }
                  })
                }}
              >
                <View style={styles.container}>
                  <Image
                    source={{
                      uri: `${BASE_URL}/${product.images[0].url}`
                    }}
                    style={{margin: 10, width: 150, height: 150}}
                  />

                  <View>
                    <View style={styles.subView2}>
                      <Text>{product.title}</Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            ))} */}
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 8,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#dcdcdc',
    width: 180,
    height: 200,
    alignItems: 'center',
    backgroundColor: '#e8ebf3',
  },
  txt: {
    fontSize: 18,
    marginTop: 10,
    color: '#000',
    textAlign: 'center',
  },
  subView: {
    flexDirection: 'row',
  },
  subContainer: {
    borderColor: '#dcdcdc',
    borderWidth: 1,
    width: 250,
    height: 100,
  },
});

export default iklanPremium;
