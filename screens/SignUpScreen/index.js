// export {default} from './SignUpScreen';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { SafeAreaView } from "react-native";
import { Text } from "react-native-paper";

const Stack = createNativeStackNavigator();

import Pakej from './Pakej';
import Form from './Form';

const SignUpScreen = () => {
  return (
    <Stack.Navigator initialRouteName="SignUpPackageScreen">
      <Stack.Screen name="SignUpPackageScreen" component={Pakej} options={{ headerShown:false }} />
      <Stack.Screen name="SignUpFormScreen" component={Form} options={{ title: 'Pendaftaran' }} />
    </Stack.Navigator>
  )
}

export default SignUpScreen;