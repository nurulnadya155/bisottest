import { SafeAreaView, ScrollView, StyleSheet, View } from "react-native";
import { ActivityIndicator, Button, RadioButton, Switch, Text, TextInput, TouchableRipple } from "react-native-paper";
import Spinner from 'react-native-loading-spinner-overlay';
import { Formik } from 'formik';
import { useRef } from "react";

const Form = ({ route, navigation }) => {
  const { packageId, packageName } = route.params;

  const ref_to_id_no_input        = useRef();
  const ref_to_company_name_input = useRef();
  const ref_to_ssm_no_input       = useRef();
  const ref_to_email_input        = useRef();
  const ref_to_telephone_input    = useRef();

  return (
    <SafeAreaView>
      <ScrollView style={styles.container}>
        <Formik
          initialValues={{
            package_id: packageId,
            registration_type_id: "1",
          }}
          onSubmit={() => {}}
        >
          {({ handleChange, handleBlur, handleSubmit, setFieldValue, isSubmitting, values }) => (
            <View>
              <Spinner
              customIndicator={<ActivityIndicator size="large" />}
              visible={isSubmitting}
              textContent={'Mendaftar...'}
              cancelable={true}
            />

              <TextInput
                label="Pakej"
                value={ packageName }
                mode="outlined"
                disabled={true}
              />

              <View style={{ marginTop: 20, marginBottom: 20 }}>
                <RadioButton.Group 
                  onValueChange={newValue => setFieldValue('registration_type_id', newValue)}
                  value={values.registration_type_id}
                >
                  <Text>Jenis Pendaftaran</Text>
                  <View style={{ flexDirection: 'row', alignContent: 'space-between' }}>
                    <View style={{ flexDirection: 'row', alignContent: 'space-between' }}>
                      <RadioButton value="1" />
                      <TouchableRipple onPress={() => setFieldValue('registration_type_id', '1')}>
                        <Text style={{ paddingTop: 6 }}>Syarikat</Text>
                      </TouchableRipple>
                    </View>
                    <View style={{ flexDirection: 'row', alignContent: 'space-between' }}>
                      <RadioButton value="2" />
                      <TouchableRipple onPress={() => setFieldValue('registration_type_id', '2')}>
                        <Text style={{ paddingTop: 6 }}>Individu</Text>
                      </TouchableRipple>
                    </View>
                  </View>
                </RadioButton.Group>
              </View>

              <TextInput
                label="Nama"
                onChangeText={handleChange('name')}
                onBlur={handleBlur('name')}
                value={values.name}
                mode="outlined"
                style={styles.txtInput}
                returnKeyType="next"
                onSubmitEditing={() => ref_to_id_no_input.current.focus()}
              />

              <TextInput
                label="No. MyKad"
                onChangeText={handleChange('id_no')}
                onBlur={handleBlur('id_no')}
                value={values.id_no}
                mode="outlined"
                style={styles.txtInput}
                ref={ref_to_id_no_input}
                keyboardType="number-pad"
                returnKeyType="next"
                onSubmitEditing={() => {
                  if (values.registration_type_id == 1) {
                    ref_to_company_name_input.current.focus();
                  } else {
                    ref_to_email_input.current.focus();
                  }
                }}
              />

              { values.registration_type_id == 1
              ? <View>
                  <TextInput
                    label="Nama Syarikat"
                    onChangeText={handleChange('company_name')}
                    onBlur={handleBlur('company_name')}
                    value={values.company_name}
                    mode="outlined"
                    style={styles.txtInput}
                    ref={ref_to_company_name_input}
                    returnKeyType="next"
                    onSubmitEditing={() => ref_to_ssm_no_input.current.focus()}
                  />

                  <TextInput
                    label="No. SSM"
                    onChangeText={handleChange('ssm_no')}
                    onBlur={handleBlur('ssm_no')}
                    value={values.ssm_no}
                    mode="outlined"
                    style={styles.txtInput}
                    ref={ref_to_ssm_no_input}
                    returnKeyType="next"
                    onSubmitEditing={() => ref_to_email_input.current.focus()}
                  />
                </View>
              : null }

              <TextInput
                label="E-mel"
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                value={values.email}
                mode="outlined"
                style={styles.txtInput}
                ref={ref_to_email_input}
                keyboardType="email-address"
                returnKeyType="next"
                onSubmitEditing={() => ref_to_telephone_input.current.focus()}
              />

              <TextInput
                label="No. Telefon"
                onChangeText={handleChange('telephone')}
                onBlur={handleBlur('telephone')}
                value={values.telephone}
                mode="outlined"
                style={styles.txtInput}
                ref={ref_to_telephone_input}
                keyboardType="phone-pad"
                returnKeyType="done"
                onSubmitEditing={handleSubmit}
              />

              <TextInput
                label="Kod Referal"
                mode="outlined"
                style={styles.txtInput}
                disabled={true}
              />

              <View style={{ flexDirection: 'row', alignContent: 'space-between', marginTop: 10, marginBottom: 15 }}>
                <Switch value={values.accept_terms} onValueChange={() => setFieldValue('accept_terms', !values.accept_terms)} />
                <TouchableRipple onPress={() => setFieldValue('accept_terms', !values.accept_terms)}>
                  <Text style={{ paddingTop: 2 }}>Saya bersetuju dengan terma & polisi</Text>
                </TouchableRipple>
              </View>

              <Button onPress={handleSubmit} mode="contained">DAFTAR</Button>

              <Text></Text>
              <Text></Text>
              <Text></Text>
            </View>
          )}
        </Formik>
      </ScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  txtInput: {
    marginBottom: 5,
  }
});

export default Form;