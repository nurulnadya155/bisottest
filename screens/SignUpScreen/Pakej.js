import {View, StyleSheet, Button, TouchableOpacity, ScrollView} from 'react-native';
import React from 'react';
import { ActivityIndicator, Divider, List, Text } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import {SafeAreaProvider, SafeAreaView} from 'react-native-safe-area-context';

import { useGetPackages } from '../../hooks/Api';

const Pakej = () => {
  const navigation = useNavigation();
  
  const { data, isLoading, isError } = useGetPackages();

  if (!data && isLoading) return <ActivityIndicator />;

  return (
    <SafeAreaView>
      <ScrollView>
        {data.map(({ id, name, attributes }, i) => (
          <View
            key={id}
            style={[styles.container0, {backgroundColor: attributes.bg_color}]}
          >
            <Text variant="titleLarge" style={{ textAlign: 'center' }}>{ attributes.role.toUpperCase() }</Text>
            <Text variant="titleLarge" style={{ textAlign: 'center' }}>{ name.toUpperCase() }</Text>
            <Text variant="headlineLarge" style={{ textAlign: 'center' }}>
              RM { parseFloat(attributes.price).toFixed(2) }
            </Text>
            {attributes.features.map((feature, i) => (
              <>
                <List.Item
                  key={id + i}
                  title={ feature }
                  titleNumberOfLines={ 5 }
                />
                <Divider style={{ backgroundColor: `dark${attributes.bg_color}`}} />
              </>
            ))}
            <View style={{ marginTop: 20 }}>
              <Button
                color="#ff2b88"
                title="Pilih"
                onPress={() => navigation.navigate('SignUpFormScreen', {
                  packageId: id,
                  packageName: attributes.role.toUpperCase() + ' ' + name.toUpperCase(),
                })}
              />
            </View>
          </View>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainTxt: {
    marginTop: 20,
    fontSize: 15,
    textAlign: 'center',
    color: 'red',
  },
  container0: {
    flexDirection: 'column',
    margin: 10,
    padding: 20,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#dcdcdc',
    backgroundColor: '#00e682',
  },
  container1: {
    margin: 10,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#dcdcdc',
    width: 370,
    height: 300,
    backgroundColor: '#4049ec',
  },
  pakejTxt: {
    margin: 20,
    fontSize: 15,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  button: {
    backgroundColor: '#ff2b88',
  },
});

export default Pakej;
