import {View, StyleSheet, ScrollView, Image, TouchableOpacity} from 'react-native';
import { ActivityIndicator, Card, Text } from 'react-native-paper';
import React, { useContext } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { SafeAreaView } from 'react-native-safe-area-context';
import { isSearchBarAvailableForCurrentPlatform } from 'react-native-screens';
import { SvgUri } from 'react-native-svg';
import { useGetClosedCategories } from '../../hooks/Api';
import Snackbar from 'react-native-snackbar';
import { useNavigation } from '@react-navigation/native';

import AuthContext from '../../context/Auth';

const UsahawanBerdaftar = () => {
  const navigation = useNavigation();

  const { state } = useContext(AuthContext);
  
  const { data, isLoading, isError } = useGetClosedCategories();

  if (!data && isLoading) return <ActivityIndicator />;

  return (
    <SafeAreaView>
      <Text style={styles.usahawanTxt}> Usahawan Berdaftar Sahaja</Text>

      <View style={styles.subView}>
        {data && data.map(({ id, name, attributes, slug, count }) => (
          <View key={id} style={styles.container}>
            <Card
              mode="contained"
              onPress={() => {
                if (typeof state.isSignout == 'undefined' || state.isSignout === true) {
                  Snackbar.show({
                    text: 'Kategori ini hanya untuk Usahawan Berdaftar Sahaja',
                    duration: Snackbar.LENGTH_SHORT,
                    action: {
                      text: 'DAFTAR',
                      onPress: () => navigation.navigate('SignUpScreen'),
                    },
                  });
                } else {
                  navigation.navigate('Produk', {
                    screen: 'SenaraiProduk',
                    params: {
                      category: slug,
                    }
                  })
                }
              }}
            >
              <Card.Content style={{ alignContent: 'center', alignItems: 'center', width: 90, height: 90 }}>
              <SvgUri
                  width="20"
                  height="20"
                  uri={'https://bisot.com.my/themes/pinlist/images/' + attributes['icon-pinlist']}
                />
                <Text style={styles.txt}>
                  {name} ({count ?? 0})
                </Text>
              </Card.Content>
            </Card>
          </View>
        ))}

        {/* {data.map(({ id, name, attributes, slug }) => (
          <TouchableOpacity
            key={id}
            onPress={() => {
              if (typeof state.isSignout == 'undefined' || state.isSignout === true) {
                Snackbar.show({
                  text: 'Kategori ini hanya untuk Usahawan Berdaftar Sahaja',
                  duration: Snackbar.LENGTH_SHORT,
                  action: {
                    text: 'DAFTAR',
                    onPress: () => navigation.navigate('SignUpScreen'),
                  },
                });
              } else {
                navigation.navigate('Produk', {
                  screen: 'SenaraiProduk',
                  params: {
                    category: slug,
                  }
                })
              }
            }}
          >
          <View style={styles.container}>
            <SvgUri
              width="20"
              height="20"
              uri={'https://bisot.com.my/themes/pinlist/images/' + attributes['icon-pinlist']}
            />
            <Text style={styles.txt}>{name}</Text>
          </View>
          </TouchableOpacity>
        ))} */}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  usahawanTxt: {
    margin: 5,
    marginTop: 30,
    fontSize: 15,
    color: '#0b5394',
  },
  container: {
    margin: 5,
    // width: 88,
    // height: 80,
    alignItems: 'center',
    // paddingVertical: 8,
    // backgroundColor: '#bcbcbc',
  },
  subView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    margin: 5,
  },
  txt: {
    fontSize: 10,
    // color: '#231a14',
    textAlign: 'center',
  },
  tinyLogo: {
    width: 20,
    height: 20,
  },
});

export default UsahawanBerdaftar;
