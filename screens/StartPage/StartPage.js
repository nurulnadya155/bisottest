import {View, Text, StyleSheet, Image} from 'react-native';
import React from 'react';

import HomeScreen from '../HomeScreen';

const StartPage = ({navigation}) => {
    // setTimeout(() => {
    //   navigation.navigate('HomeScreen')
    // }, 3000);
  
  return (
    <View style={styles.container}>
      <View>
      <Text style={styles.Txt}> Laman E-Dagang Terbesar</Text>
      </View>
      
      <View>
        <Image source={require('../../assets/images/Bisot_Logo.jpeg')}
        style={styles.image} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    backgroundColor: '#3ba2ff',
  },
  Txt:{
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 25,
    fontStyle: 'italic'
  },
  image: {
    width: 300,
    height: 90,
  },

});

export default StartPage;
