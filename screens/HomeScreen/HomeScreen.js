import {View, StyleSheet, Text, ScrollView} from 'react-native';
import React from 'react';
import UsahawanBerdaftar from '../UsahawanBerdaftar';
import IconLain from '../IconLain';
import IklanPremium from '../IklanPremium';
import {SafeAreaView} from 'react-native-safe-area-context';
// import ButtonNavigation from '../../components/CustomHeader';

const HomeScreen = () => { 
  return (
    <SafeAreaView>
      <ScrollView vertical={true} showsVerticalScrollIndicator={false}>
        {/* <ButtonNavigation
          headerBg="#3ba2ff"
          iconColor="#000000"
          menu
          right="heart"
          rightFunction={() => console.log('right')}
          optionalIcon="bookmark"
          optionalFunc={() => console.log('optional')}
        /> */}

        <Text style={styles.mainTxt}>Laman E- Dagang Terbesar</Text>
        <IklanPremium />
        <IconLain />
        <UsahawanBerdaftar />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainTxt: {
    margin: 10,
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
  },
});

export default HomeScreen;
