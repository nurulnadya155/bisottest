import React, {useContext, useEffect, useState} from 'react';
import {View, SafeAreaView, StyleSheet, ScrollView} from 'react-native';
import {Button, Text} from 'react-native-paper';
import Stats from './Stats/Stats';

import AuthContext from '../../context/Auth';

const Dashboard = ({ navigation }) => {
  const { state } = useContext(AuthContext);

  return (
    <SafeAreaView style={{ backgroundColor: '#fff' }}>
      <ScrollView>
        <Text style={{ fontSize: 15, padding: 20 }}>
          Selamat Datang, {state && state.user && state.user.name ? state.user.name : 'pengguna'}!
        </Text>
        <View style={{ margin:8, alignItems:'center' }}>
          <Text style={{ color: 'red', fontSize:10 }}>
            Tiada penambahan pakej buat sementara waktu
          </Text>
        </View>
        <Stats />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({});

export default Dashboard;
