import {Dimensions, View, Text, StyleSheet, ScrollView, SafeAreaView} from 'react-native';
import React, { useEffect, useState } from 'react';
import {ActivityIndicator, TextInput, Switch,TouchableRipple, Button, Checkbox, Avatar, HelperText, Image} from 'react-native-paper';
import { DatePickerModal } from 'react-native-paper-dates';
import Icon from 'react-native-vector-icons/Feather';
import Snackbar from 'react-native-snackbar';
import Spinner from 'react-native-loading-spinner-overlay';
import { Formik } from 'formik';
import { PaperSelect } from 'react-native-paper-select';
import { launchImageLibrary } from 'react-native-image-picker';

import { BASE_URL, useCities, useDistricts, useLookups, useMalaysiaState } from '../../../hooks/Api';
import { useEvent } from '../../../hooks/PrivateApi';

import moment from 'moment';
import dist from 'swr';

const FormAcara = ({ route }) => {
  const eventId = route?.params?.eventId;

  const windowWidth = Dimensions.get('window').width - 40;

  const [openCalendar, setOpenCalendar] = useState(false);

  const [images, setImages] = useState([]);

  const { data, isLoading, isError } = useEvent(eventId);

  const eventTypes = useLookups('event-types');

  const [postcode, setPostcode] = useState(data?.postcode)

  const malaysianStates = useMalaysiaState(postcode);
  const [malaysianState, setStateList] = useState({
    value: data?.state_code,
    list: [],
    selectedList: [{ _id: data?.state_code, value: data?.state_code }],
    error: '',
  });

  const districts = useDistricts(postcode);
  const [district, setDistrictList] = useState({
    value: data?.district,
    list: [],
    selectedList: [{ _id: data?.district, value: data?.district }],
    error: '',
  });

  const cities = useCities(postcode);
  const [city, setCityList] = useState({
    value: data?.city,
    list: [],
    selectedList: [{ _id: data?.city, value: data?.city }],
    error: '',
  });

  const [eventType, setEventType] = useState({
    value: '',
    list: [],
    selectedList: [{ _id: data?.event_type?.id, value: data?.event_type?.name }],
    error: '',
  });

  useEffect(() => {
    if (eventTypes && eventTypes.data && eventTypes.data.data) {
      eventTypes.data.data.map((eventType) => {
        eventType.value = eventType.name;
        eventType._id   = eventType.id;
      });

      setEventType({
        ...eventType,
        list: eventTypes.data.data
      });
    }

    return () => false;
  }, [eventTypes?.data?.data]);

  useEffect(() => {
    if (malaysianStates && malaysianStates.data && malaysianStates.data.data) {
      malaysianStates.data.data.map((state) => {
        state.value = state.text;
        state._id   = state.id;
      });

      setStateList({
        ...malaysianState,
        list: malaysianStates.data.data
      });
    }

    return () => false;
  }, [malaysianStates?.data?.data]);

  useEffect(() => {
    if (districts && districts.data && districts.data.data) {
      districts.data.data.map((district) => {
        district.value = district.text;
        district._id   = district.id;
      });

      setDistrictList({
        ...district,
        list: districts.data.data
      });
    }

    return () => false;
  }, [districts?.data?.data]);

  useEffect(() => {
    if (cities && cities.data && cities.data.data) {
      cities.data.data.map((city) => {
        city.value = city.text;
        city._id   = city.id;
      });

      setCityList({
        ...city,
        list: cities.data.data
      });
    }

    return () => false;
  }, [cities?.data?.data]);

  useEffect(() => {
    if (data && data.postcode) {
      setPostcode(data.postcode);
    }
  }, [data?.postcode]);

  useEffect(() => {
    if (data && data.event_type) {
      setEventType({
        ...eventType,
        value: data.event_type.name,
        selectedList: [
          {
            value: data.event_type.name,
            _id: data.event_type.id,
          }
        ]
      });
    }
  }, [data?.event_type]);

  useEffect(() => {
    if (data && data.state_code) {
      setStateList({
        ...malaysianState,
        value: data.state_code,
        selectedList: [
          {
            value: data.state_code,
            _id: data.state_code,
          }
        ]
      });
    }
  }, [data?.state_code]);

  useEffect(() => {
    if (data && data.district) {
      setDistrictList({
        ...district,
        value: data.district,
        selectedList: [
          {
            value: data.district,
            _id: data.district,
          }
        ]
      });
    }
  }, [data?.district]);

  useEffect(() => {
    if (data && data.city) {
      setCityList({
        ...city,
        value: data.city,
        selectedList: [
          {
            value: data.city,
            _id: data.city,
          }
        ]
      });
    }
  }, [data?.city]);

  if (isLoading) {
    return <ActivityIndicator />
  }

  return (
    <SafeAreaView
      style={{backgroundColor: '#fff', height: '100%'}}
    >
      <ScrollView style={{ padding: 20 }}>
        <Formik
          enableReinitialize={true}
          initialValues={data}
          onSubmit={(values, actions) => {
            setTimeout(() => {
              actions.setSubmitting(false);
            }, 1000);

            setTimeout(() => {
              Snackbar.show({
                text: 'Ralat, simpan data tidak berjaya',
                duration: Snackbar.LENGTH_SHORT,
              });
            }, 1200);
          }}
        >
          {({ handleChange, handleBlur, handleSubmit, setFieldValue, isSubmitting, values }) => (
            <>
              <Spinner
                customIndicator={<ActivityIndicator size="large" />}
                visible={isSubmitting}
                textContent={'Saving...'}
                cancelable={true}
              />

              <TextInput
                label="Nama Acara"
                style={styles.textInput}
                mode="outlined"
                multiline={true}
                onChangeText={handleChange('name')}
                onBlur={handleBlur('name')}
                value={values?.name}
              />

              <PaperSelect
                label="Jenis Acara"
                value={eventType.value}
                onSelection={(value) => {
                  setEventType({
                    ...eventType,
                    value: value.text,
                    selectedList: value.selectedList,
                    error: '',
                  });

                  setFieldValue('type', value.selectedList[0]._id);
                }}
                arrayList={[...eventType.list]}
                selectedArrayList={[...eventType.selectedList]}
                errorText={eventType.error}
                multiEnable={false}
              />

              {/* <DropDown
                label={"Jenis Acara"}
                mode={"outlined"}
                visible={showEventTypeDropdown}
                showDropDown={() => setShowEventTypeDropdown(true)}
                onDismiss={() => setShowEventTypeDropdown(false)}
                value={values?.type}
                setValue={(value) => setFieldValue('type', value)}
                list={eventTypesList}
              /> */}

              <TextInput
                label="Maklumat Acara"
                style={styles.textInput}
                mode="outlined"
                multiline={true}
                numberOfLines={5}
                onChangeText={handleChange('description')}
                onBlur={handleBlur('description')}
                value={values?.description}
              />

              <TextInput
                label="Alamat Acara"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('address')}
                onBlur={handleBlur('address')}
                value={values?.address}
              />

              <TextInput
                label="Poskod"
                style={styles.textInput}
                mode="outlined"
                onChangeText={(value) => {
                  setFieldValue('postcode', value);

                  setPostcode(value);
                }}
                onBlur={handleBlur('postcode')}
                value={values?.postcode}
              />

              <PaperSelect
                label="Negeri"
                value={malaysianState.value}
                onSelection={(value) => {
                  setStateList({
                    ...malaysianState,
                    value: value.text,
                    selectedList: value.selectedList,
                    error: '',
                  });

                  setDistrictList({
                    ...district,
                    value: '',
                    selectedList: [{ _id: null, value: '' }],
                    error: '',
                  });

                  setCityList({
                    ...city,
                    value: '',
                    selectedList: [{ _id: null, value: '' }],
                    error: '',
                  });

                  setFieldValue('state_code', value.selectedList[0]._id);
                }}
                arrayList={[...malaysianState.list]}
                selectedArrayList={[...malaysianState.selectedList]}
                errorText={malaysianState.error}
                multiEnable={false}
              />

              <PaperSelect
                label="Daerah"
                value={district.value}
                onSelection={(value) => {
                  setDistrictList({
                    ...district,
                    value: value.text,
                    selectedList: value.selectedList,
                    error: '',
                  });

                  setCityList({
                    ...city,
                    value: '',
                    selectedList: [{ _id: null, value: '' }],
                    error: '',
                  });

                  setFieldValue('district', value.selectedList[0]._id);
                }}
                arrayList={[...district.list]}
                selectedArrayList={[...district.selectedList]}
                errorText={district.error}
                multiEnable={false}
              />
              
              <PaperSelect
                label="Bandar"
                value={city.value}
                onSelection={(value) => {
                  setCityList({
                    ...city,
                    value: value.text,
                    selectedList: value.selectedList,
                    error: '',
                  });

                  setFieldValue('city', value.selectedList[0]._id);
                }}
                arrayList={[...city.list]}
                selectedArrayList={[...city.selectedList]}
                errorText={city.error}
                multiEnable={false}
              />

              {/* <DropDown
                label={"Negeri"}
                mode={"outlined"}
                visible={showStatesListDropdown}
                showDropDown={() => setStatesListDropdown(true)}
                onDismiss={() => setStatesListDropdown(false)}
                value={values?.state_code}
                setValue={(value) => setFieldValue('state_code', value)}
                list={statesList}
              />

              <DropDown
                label={"Daerah"}
                mode={"outlined"}
                visible={showDistrictsListDropdown}
                showDropDown={() => setShowDistrictsListDropdown(true)}
                onDismiss={() => setShowDistrictsListDropdown(false)}
                value={values?.district}
                setValue={(value) => setFieldValue('district', value)}
                list={districtsList}
              />

              <DropDown
                label={"Bandar"}
                mode={"outlined"}
                visible={showCitiesListDropdown}
                showDropDown={() => setShowCitiesListDropdown(true)}
                onDismiss={() => setShowCitiesListDropdown(false)}
                value={values?.city}
                setValue={(value) => setFieldValue('city', value)}
                list={citiesList}
              /> */}

              <DatePickerModal
                // locale="en"
                mode="range"
                visible={openCalendar}
                onDismiss={() => setOpenCalendar(false)}
                // startDate={range.startDate}
                // endDate={range.endDate}
                onConfirm={() => {
                  setOpenCalendar(false);
                }}
              />

              <TextInput
                label="Tarikh Acara"
                style={styles.textInput}
                mode="outlined"
                value={moment(values?.start_date).format('DD/MM/Y') + ' - ' + moment(values?.end_date).format('DD/MM/Y')}
                onFocus={() => setOpenCalendar(true)}
              />

              <View style={{ flexDirection: 'row' }}>
                <Checkbox 
                  status={values?.has_fees ? 'checked' : 'unchecked'}
                  onPress={() => setFieldValue('has_fees', !values?.has_fees)}
                />
                <Text 
                  style={{ paddingTop: 8 }}
                  onPress={() => setFieldValue('has_fees', !values?.has_fees)}  
                >Bayaran Dikenakan</Text>
              </View>

              <TextInput
                label="Bayaran (RM)"
                style={[styles.textInput, {marginBottom: 20}]}
                mode="outlined"
                onChangeText={handleChange('fees')}
                onBlur={handleBlur('fees')}
                value={values?.fees}
                disabled={values?.has_fees ? false : true}
              />

              <Text variant="bodyLarge">Gambar Acara</Text>
              {data && data.banner_image && data.banner_image.url
              ? <Image 
              source={{ uri: BASE_URL + '/' + data.banner_image.url }} 
              style={{ height: windowWidth / 4, width: windowWidth / 4}} 
            />
              : <TouchableRipple
                  onPress={() => {
                    launchImageLibrary({
                      mediaType: 'photo',
                      saveToPhotos: false,
                    }, ({ assets, errorCode, errorMessage }) => {
                      if (assets && assets[0]) {
                        setImages([
                          ...images,
                          assets[0],
                        ]);
                      }

                      if (errorMessage) {
                        Snackbar.show({
                          text: errorMessage,
                          duration: Snackbar.LENGTH_SHORT,
                        });
                      }
                    });
                  }}
                  style={{ marginBottom: 20 }}
                >
                  <Icon name="image" size={windowWidth / 4} color="#999" />
                </TouchableRipple>
              }

              <View style={{ flexDirection: 'row' }}>
                <Checkbox 
                  status={values?.send_for_approval ? 'checked' : 'unchecked'}
                  onPress={() => setFieldValue('send_for_approval', !values?.send_for_approval)}
                />
                <Text 
                  style={{ paddingTop: 8 }}
                  onPress={() => setFieldValue('send_for_approval', !values?.send_for_approval)}
                >
                  Hantar Untuk Kelulusan
                </Text>
              </View>
              <HelperText style={{ marginBottom: 20 }}>
                acara tidak boleh diubah suai selepas permohonan diluluskan oleh admin
              </HelperText>

              <Button 
                mode="contained" 
                style={{ marginBottom: 40 }}
                disabled={values?.state == 'Approved' || isSubmitting}
                onPress={handleSubmit}
              >
                {data?.id ? 'Kemaskini' : 'Hantar'}
              </Button>
            </>
          )}
        </Formik>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  modal: {
    margin: 5,
  },
  textHeader: {
    margin: 5,
    color: '#000',
  },
  textInput: {
    marginBottom: 5,
  },
});

export default FormAcara;
