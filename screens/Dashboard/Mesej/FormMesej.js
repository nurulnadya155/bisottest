import {View, Text, StyleSheet, ScrollView, SafeAreaView, KeyboardAvoidingView} from 'react-native';
import React from 'react';
import {TextInput, Switch, TouchableRipple, Button} from 'react-native-paper';

const FormMesej = () => {
  const [isSwitchOn, setIsSwitchOn] = React.useState(false);

  const onToggleSwitch = () => setIsSwitchOn(!isSwitchOn);

  return (
    <SafeAreaView style={{backgroundColor: '#fff', height: '100%'}}>
      <KeyboardAvoidingView>
        {/* <View>
          <Text style={{fontSize: 18, marginBottom: 15}}> Mesej Baru</Text>
        </View> */}

        <View style={{justifyContent: 'center', padding: 20}}>
          
          <TextInput
            label="Penerima"
            style={styles.textInput}
            mode="outlined"
            returnKeyType="next"
          />
          <TextInput
            label="Tajuk Mesej"
            style={styles.textInput}
            mode="outlined"
            returnKeyType="next"
          />
          <TextInput
            label="Mesej"
            style={styles.textMesej}
            multiline={true}
            numberOfLines={5} 
            maxLength={250}
            mode="outlined"
            returnKeyType="next"
          />

          <View style={{marginTop: 15, justifyContent: 'space-evenly'}}>  
            <Button mode="contained">Hantar</Button>
          </View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  modal: {
    margin: 5,
  },
  textHeader: {
    margin: 5,
    color: '#000',
  },
  textInput: {
    marginBottom: 5,
  },
  textMesej:{
    height: 100,
  },
});

export default FormMesej;
