import {View, Text, StyleSheet, ScrollView, SafeAreaView, KeyboardAvoidingView} from 'react-native';
import React, { useContext } from 'react';
import {ActivityIndicator, TextInput, Switch, TouchableRipple, Button} from 'react-native-paper';
import { Formik } from 'formik';
import Snackbar from 'react-native-snackbar';
import Spinner from 'react-native-loading-spinner-overlay';

import AuthContext from '../../../context/Auth';
import { BASE_URL } from '../../../hooks/Api';

const ReplyFormMesej = ({ navigation, route }) => {
  const threadId = route?.params?.threadId;

  const { state } = useContext(AuthContext);

  return (
    <SafeAreaView style={{backgroundColor: '#fff', height: '100%'}}>
      <KeyboardAvoidingView>
        {/* <View>
          <Text style={{fontSize: 18, marginBottom: 15}}> Mesej Baru</Text>
        </View> */}

        <Formik
          enableReinitialize={true}
          initialValues={{
            threadId: threadId,
          }}
          onSubmit={async (values, actions) => {
            if (values && values.message && values.message.length == 0 || values.message === undefined) {
              Snackbar.show({
                text: 'Sila masukkan mesej anda',
                duration: Snackbar.LENGTH_SHORT,
              });

              return;
            }

            const response = await fetch(`${BASE_URL}/api/admin/messages/${values.threadId}`, {
              method: 'PUT',
              headers: {
                'Accept'          : 'application/json',
                'Authorization'   : 'Bearer ' + state.userToken,
                'Content-Type'    : 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
              },
              body: JSON.stringify(values),
            })
    
            const json = await response.json();

            if (json && json.success) {
              navigation.navigate('ViewMesejScreen', {
                threadId: values.threadId
              });

              return;
            }

            Snackbar.show({
              text: 'Ralat, tidak dapat menghantar mesej',
              duration: Snackbar.LENGTH_SHORT,
            });
          }}
        >
          {({ handleChange, handleBlur, handleSubmit, setFieldValue, isSubmitting, values }) => (
            <View style={{justifyContent: 'center', padding: 20}}>
              <Spinner
                customIndicator={<ActivityIndicator size="large" />}
                visible={isSubmitting}
                textContent={'Saving...'}
                cancelable={true}
              />

              <TextInput
                label="Mesej"
                style={styles.textMesej}
                multiline={true}
                numberOfLines={5} 
                maxLength={250}
                mode="outlined"
                returnKeyType="next"
                onChangeText={handleChange('message')}
                onBlur={handleBlur('message')}
                value={values?.message}
              />

              <View style={{marginTop: 15, justifyContent: 'space-evenly'}}>  
                <Button 
                  mode="contained"
                  disabled={isSubmitting}
                  onPress={handleSubmit}
                >
                  Hantar
                </Button>
              </View>
            </View>
          )}
        </Formik>
        
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  modal: {
    margin: 5,
  },
  textHeader: {
    margin: 5,
    color: '#000',
  },
  textInput: {
    marginBottom: 5,
  },
  textMesej:{
    height: 100,
  },
});

export default ReplyFormMesej;
