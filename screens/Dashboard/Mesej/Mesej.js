import React from 'react';
import { RefreshControl, SafeAreaView, ScrollView, StyleSheet, View, Image } from 'react-native';
import { Chip, Divider, FAB, List, Text } from 'react-native-paper';

import { useMessageIndex } from '../../../hooks/PrivateApi';

import moment from "moment";

const Mesej = ({ navigation }) => {
  const { data, isLoading, isError } = useMessageIndex();

  const onRefresh = React.useCallback(() => {});

  return (
    <SafeAreaView style={{backgroundColor:'#fff', height: '100%'}}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={onRefresh} />
        }
      >
        {data && data.map(({ id, subject, latest_message, creator, is_unread }, i) => (
          <View key={i} style={{ paddingLeft: 10}}>
            <List.Item
              title={creator?.name}
              titleStyle={{ fontWeight: is_unread ? 'bold' : 'normal' }}
              description={(prop) => <Text style={{ fontWeight: is_unread ? 'bold' : 'normal' }}>{subject}: {latest_message?.body}</Text>}
              left={(prop) => <List.Image variant="image" source={{ uri: 'https://ui-avatars.com/api/?background=0D8ABC&color=fff&name=' + creator?.name }} />}
              right={(prop) => is_unread ? <Chip>Baru</Chip> : <Text>{moment(latest_message.created_at).fromNow()}</Text>}
              onPress={() => navigation.navigate('ViewMesejScreen', {
                threadId: id
              })}
            />
            <Divider />
          </View>
        ))}
      </ScrollView>
      <FAB
        icon="plus"
        style={styles.fab}
        onPress={() => navigation.navigate('FormMesejScreen')}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

export default Mesej;