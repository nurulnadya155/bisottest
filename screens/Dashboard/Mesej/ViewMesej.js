import React, { useEffect } from "react";
import { SafeAreaView, ScrollView, View } from "react-native";
import { ActivityIndicator, Button, Card, Text, TouchableRipple } from "react-native-paper";
import Feather from 'react-native-vector-icons/Feather';

import { useMessage } from "../../../hooks/PrivateApi";

import moment from "moment";

const ViewMesej = ({ navigation, route }) => {
  const threadId = route?.params?.threadId;

  const { data, isLoading, isError } = useMessage(threadId);

  useEffect(() => {
    navigation.setOptions({
      title: data?.subject ?? 'Lihat Mesej',
      headerRight: () => (
        <TouchableRipple
          onPress={() => navigation.navigate('ReplyMesejScreen', {
            threadId: data?.id
          })}
        >
          <Feather name="corner-up-left" size={24} color="#000"></Feather>
        </TouchableRipple>
      )
    });
  }, [data]);

  if (isLoading) {
    return <ActivityIndicator animating={true} size="large" />
  }

  return (
    <SafeAreaView style={{ height: '100%' }}>
      <ScrollView style={{ backgroundColor: "#fff", padding: 20 }}>
        {data && data.messages && data.messages.map(({ body, created_at, user }, i) => (
          <Card key={i} style={{ marginBottom: 20 }}>
            <Card.Content>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text>{user?.name}</Text>
                <Text>{moment(created_at).fromNow()}</Text>
              </View>
              <Text>{body}</Text>
            </Card.Content>
          </Card>
        ))}
      </ScrollView>
    </SafeAreaView>
  )
}

export default ViewMesej;