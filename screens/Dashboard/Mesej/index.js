import React from 'react';

import FormMesej from './FormMesej';
import Mesej from './Mesej';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ReplyFormMesej from './ReplyFormMesej';


const Stack = createNativeStackNavigator();

const MessagesStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Mesej" component={Mesej} options={{ headerShown: true, title: 'Mesej' }} />
      <Stack.Screen name="FormMesej" component={FormMesej} options={{ headerShown: true, title: 'Mesej' }} />
      <Stack.Screen name="ReplyFormMesej" component={ReplyFormMesej} options={{ headerShown: true, title: 'Balas Mesej' }} />
    </Stack.Navigator>
  )
}

export default MessagesStack;