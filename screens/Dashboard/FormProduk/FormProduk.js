import {Dimensions, View, StyleSheet, ScrollView, SafeAreaView, Image} from 'react-native';
import React, { useEffect, useState } from 'react';
import {ActivityIndicator, TextInput, Text, Switch, TouchableRipple, Button,List, Checkbox, HelperText} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Feather';
import Spinner from 'react-native-loading-spinner-overlay';
import { launchImageLibrary } from 'react-native-image-picker';
import Snackbar from 'react-native-snackbar';
import { Formik } from 'formik';
import { PaperSelect } from 'react-native-paper-select';

import { BASE_URL, useLookups } from '../../../hooks/Api';
import { useProduct } from '../../../hooks/PrivateApi';

const FormProduk = ({ route }) => {
  const productId = route?.params?.productId;

  const [images, setImages] = useState([]);

  const windowWidth = Dimensions.get('window').width - 40;

  const { data, isLoading, isError } = useProduct(productId);

  const [selectedGroupCategory, setSelectedGroupCategory] = useState('');
  const groupCategories = useLookups('group-categories');

  const categories = useLookups('categories', selectedGroupCategory);

  const productTypes = useLookups('product-types');

  const [groupCategory, setGroupCategory] = useState({
    value: data?.group_category?.name,
    list: [],
    selectedList: [{ _id: data?.group_category?.id, value: data?.group_category?.name }],
    error: '',
  });

  const [category, setCategory] = useState({
    value: data?.category?.name,
    list: [],
    selectedList: [{ _id: data?.category?.id, value: data?.category?.name }],
    error: '',
  });

  useEffect(() => {
    if (categories && categories.data && categories.data.data) {
      categories.data.data.map((category) => {
        category.value = category.name;
        category._id   = category.id;
      });

      setCategory({
        ...category,
        list: categories.data.data
      });
    }
  }, [categories?.data?.data, selectedGroupCategory]);

  useEffect(() => {
    if (groupCategories && groupCategories.data && groupCategories.data.data) {
      groupCategories.data.data.map((groupCategory) => {
        groupCategory.value = groupCategory.name;
        groupCategory._id   = groupCategory.id;
      });
    
      setGroupCategory({
        ...groupCategory,
        list: groupCategories.data.data
      });
    }
  }, [groupCategories?.data?.data]);

  useEffect(() => {
    if (data && data.group_category) {
      setGroupCategory({
        ...groupCategory,
        value: data.group_category.name,
        selectedList: [
          {
            value: data.group_category.name,
            _id: data.group_category.id,
          }
        ]
      });

      setSelectedGroupCategory(data.group_category.id);
    }
  }, [data?.group_category]);

  useEffect(() => {
    if (data && data.category) {
      setCategory({
        ...category,
        value: data.category.name,
        selectedList: [
          {
            value: data.category.name,
            _id: data.category.id,
          }
        ]
      });
    }
  }, [data?.category]);

  if (isLoading) {
    return <ActivityIndicator />
  }

  return (
    <SafeAreaView
      style={{backgroundColor: '#fff', height: '100%'}}
    >
      <ScrollView style={{ padding: 20 }}>
        <Formik
          enableReinitialize={true}
          initialValues={data}
          onSubmit={(values, actions) => {
            setTimeout(() => {
              actions.setSubmitting(false);
            }, 1000);

            setTimeout(() => {
              Snackbar.show({
                text: 'Ralat, simpan data tidak berjaya',
                duration: Snackbar.LENGTH_SHORT,
              });
            }, 1200);
          }}
        >
          {({ handleChange, handleBlur, handleSubmit, setFieldValue, isSubmitting, values }) => (
            <>
              <Spinner
                customIndicator={<ActivityIndicator size="large" />}
                visible={isSubmitting}
                textContent={'Saving...'}
                cancelable={true}
              />

              <Text variant="bodyLarge">Gambar Produk</Text>

              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              
                {data && data.images && data.images[0]
                ? <Image 
                  source={{ uri: BASE_URL + '/' + data.images[0].url }} 
                  style={{ height: windowWidth / 4, width: windowWidth / 4}} 
                />
                : <TouchableRipple
                    onPress={() => {
                      launchImageLibrary({
                        mediaType: 'photo',
                        saveToPhotos: false,
                      }, ({ assets, errorCode, errorMessage }) => {
                        if (assets && assets[0]) {
                          setImages([
                            ...images,
                            assets[0],
                          ]);
                        }

                        if (errorMessage) {
                          Snackbar.show({
                            text: errorMessage,
                            duration: Snackbar.LENGTH_SHORT,
                          });
                        }
                      });
                    }}
                    style={{ marginBottom: 20 }}
                  >
                    <Icon name="image" size={windowWidth / 4} color="#999" />
                  </TouchableRipple>
                }

              {data && data.images && data.images[1]
              ? <Image 
                source={{ uri: BASE_URL + '/' + data.images[1].url }} 
                style={{ height: windowWidth / 4, width: windowWidth / 4}} 
              />
              : <TouchableRipple
                  onPress={() => {
                    launchImageLibrary({
                      mediaType: 'photo',
                      saveToPhotos: false,
                    }, ({ assets, errorCode, errorMessage }) => {
                      if (assets && assets[0]) {
                        setImages([
                          ...images,
                          assets[0],
                        ]);
                      }

                      if (errorMessage) {
                        Snackbar.show({
                          text: errorMessage,
                          duration: Snackbar.LENGTH_SHORT,
                        });
                      }
                    });
                  }}
                  style={{ marginBottom: 20 }}
                >
                  <Icon name="image" size={windowWidth / 4} color="#999" />
                </TouchableRipple>
              }

              {data && data.images && data.images[2]
              ? <Image 
                source={{ uri: BASE_URL + '/' + data.images[2].url }} 
                style={{ height: windowWidth / 4, width: windowWidth / 4}} 
              />
              : <TouchableRipple
                  onPress={() => {
                    launchImageLibrary({
                      mediaType: 'photo',
                      saveToPhotos: false,
                    }, ({ assets, errorCode, errorMessage }) => {
                      if (assets && assets[0]) {
                        setImages([
                          ...images,
                          assets[0],
                        ]);
                      }

                      if (errorMessage) {
                        Snackbar.show({
                          text: errorMessage,
                          duration: Snackbar.LENGTH_SHORT,
                        });
                      }
                    });
                  }}
                  style={{ marginBottom: 20 }}
                >
                  <Icon name="image" size={windowWidth / 4} color="#999" />
                </TouchableRipple>
              }

              {data && data.images && data.images[3]
              ? <Image 
                source={{ uri: BASE_URL + '/' + data.images[3].url }} 
                style={{ height: windowWidth / 4, width: windowWidth / 4}} 
              />
              : <TouchableRipple
                  onPress={() => {
                    launchImageLibrary({
                      mediaType: 'photo',
                      saveToPhotos: false,
                    }, ({ assets, errorCode, errorMessage }) => {
                      if (assets && assets[0]) {
                        setImages([
                          ...images,
                          assets[0],
                        ]);
                      }

                      if (errorMessage) {
                        Snackbar.show({
                          text: errorMessage,
                          duration: Snackbar.LENGTH_SHORT,
                        });
                      }
                    });
                  }}
                  style={{ marginBottom: 20 }}
                >
                  <Icon name="image" size={windowWidth / 4} color="#999" />
                </TouchableRipple>
              }
              </View>

              <TextInput
                label="Nama Produk/Perkhidmatan"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('title')}
                onBlur={handleBlur('title')}
                value={values?.title}
                maxLength={120}
              />
              <HelperText style={{ marginBottom: 20 }}>Maksimum 120 aksara</HelperText>

              <TextInput
                label="Penerangan"
                style={styles.textInput}
                mode="outlined"
                multiline={true}
                numberOfLines={5}
                onChangeText={handleChange('description')}
                onBlur={handleBlur('description')}
                value={values?.description}
                maxLength={3000}
              />
              <HelperText style={{ marginBottom: 20 }}>Maksimum 3000 aksara</HelperText>

              <PaperSelect
                label="Kategori Produk/Perkhidmatan"
                value={groupCategory.value}
                onSelection={(value) => {
                  setGroupCategory({
                    ...groupCategory,
                    value: value.text,
                    selectedList: value.selectedList,
                    error: '',
                  });

                  setCategory({
                    ...category,
                    value: '',
                    selectedList: [{ _id: null, value: '' }],
                    error: '',
                  });
                  setSelectedGroupCategory(value.selectedList[0]._id);

                  setFieldValue('group_category_id', value.selectedList[0]._id);
                  setFieldValue('category_id', '');

                  categories.mutate();
                }}
                arrayList={[...groupCategory.list]}
                selectedArrayList={[...groupCategory.selectedList]}
                errorText={groupCategory.error}
                multiEnable={false}
              />

              <PaperSelect
                label="Jenis Kategori"
                value={category.value}
                onSelection={(value) => {
                  setCategory({
                    ...category,
                    value: value.text,
                    selectedList: value.selectedList,
                    error: '',
                  });

                  setFieldValue('category_id', value);
                }}
                arrayList={[...category.list]}
                selectedArrayList={[...category.selectedList]}
                errorText={category.error}
                multiEnable={false}
              />
              
              <TextInput
                label="Pautan Shopee"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('shopee_link')}
                onBlur={handleBlur('shopee_link')}
                value={values?.shopee_link}
              />

              <Text variant="bodyLarge">Kategori</Text>
              <View style={{ flexDirection: 'column', flexWrap: 'wrap' }}>
                {productTypes && productTypes.data && productTypes.data.data
                ? productTypes.data.data.map((productType, i) => (
                  <View key={i} style={{ flexDirection: 'row' }}>
                    <Checkbox />
                    <Text 
                      style={{ paddingTop: 8 }}
                      
                    >
                      {productType?.name}
                    </Text>
                  </View>
                ))
                : null}
              </View>
              <HelperText style={{ marginBottom: 20 }}>sila pilih satu atau semua kategori</HelperText>

              <Text>{values?.has_agent}</Text>
              <View style={{ flexDirection: 'row' }}>
                <Checkbox 
                  status={values?.has_agent ? 'checked' : 'unchecked'}
                  onPress={() => setFieldValue('has_agent', !values?.has_agent)}
                />
                <Text 
                  style={{ paddingTop: 8 }}
                  onPress={() => setFieldValue('has_agent', !values.has_agent)}
                >
                  Ejen
                </Text>
              </View>
              <HelperText style={{ marginBottom: 20 }}>klik jika anda mencari ejen</HelperText>

              {values?.has_agent
              ? <>
                <TextInput
                  label="Harga Ejen"
                  style={styles.textInput}
                  mode="outlined"
                  onChangeText={(value) => {
                    setFieldValue('agent_price', value);
                  }}
                  onBlur={handleBlur('agent_price')}
                  value={values?.agent_price?.toString()}
                />

                <TextInput
                  label="MOQ"
                  style={styles.textInput}
                  mode="outlined"
                  onChangeText={(value) => {
                    setFieldValue('moq', value);
                  }}
                  onBlur={handleBlur('moq')}
                  value={values?.moq?.toString()}
                />

                <TextInput
                  label="Jumlah Modal (Harga Ejen × MOQ)"
                  style={styles.textInput}
                  mode="outlined"
                  value={(parseFloat(values?.agent_price) * parseInt(values?.moq))?.toString()}
                  disabled={true}
                />
              </> : null}

              <View style={{ flexDirection: 'row' }}>
                <Checkbox 
                  status={values?.send_for_approval ? 'checked' : 'unchecked'}
                  onPress={() => setFieldValue('send_for_approval', !values?.send_for_approval)}
                />
                <Text 
                  style={{ paddingTop: 8 }}
                  onPress={() => setFieldValue('send_for_approval', !values.send_for_approval)}
                >
                  Hantar Untuk Kelulusan
                </Text>
              </View>
              <HelperText style={{ marginBottom: 20 }}>
                produk tidak boleh diubah suai selepas permohonan diluluskan oleh admin
              </HelperText>

              <Button 
                mode="contained" 
                style={{ marginBottom: 40 }}
                disabled={values?.state == 'Approved' || isSubmitting}
                onPress={handleSubmit}
              >
                {data?.id ? 'Kemaskini' : 'Hantar'}
              </Button>
            </>
          )}
        </Formik>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  modal: {
    margin: 5,
  },
  textHeader: {
    margin: 5,
    color: '#000',
  },
  textInput: {
    marginBottom: 5,
  },
});

export default FormProduk;
