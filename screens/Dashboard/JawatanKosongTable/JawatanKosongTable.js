import {View, StyleSheet, Text} from 'react-native';
import React from 'react';
import {Divider, FAB, List, Searchbar, TouchableRipple} from 'react-native-paper';
import {SafeAreaView} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

import { BASE_URL } from '../../../hooks/Api';
import { useJobVacancyIndex } from '../../../hooks/PrivateApi';

import moment from 'moment';
import { useNavigation } from '@react-navigation/native';

const JawatanKosongTable = ({ route }) => {
  const status = route?.params?.status;

  const navigation = useNavigation();

  const [searchQuery, setSearchQuery] = React.useState('');

  const onChangeSearch = query => setSearchQuery(query);

  const [page, setPage] = React.useState(0);

  const { data, isLoading, isError } = useJobVacancyIndex(searchQuery, status, page);

  // const [numberOfItemsPerPage, onItemsPerPageChange] = React.useState(
  //   numberOfItemsPerPageList[0],
  // );
  // const from = page * numberOfItemsPerPage;
  // const to = Math.min((page + 1) * numberOfItemsPerPage, items.length);

  // React.useEffect(() => {
  //   setPage(0);
  // }, [numberOfItemsPerPage]);

  return (
    <SafeAreaView style={{backgroundColor:'#fff', height: '100%'}}>
      <ScrollView>
        <Searchbar
        style={{backgroundColor:'#fff'}}
          placeholder="Cari jawatan kosong"
          onChangeText={onChangeSearch}
          value={searchQuery}
        />
        <View>
          <Text style={{color: 'red', fontSize:11, marginTop:10}}>
            Tiada penambahan pakej buat sementara waktu
          </Text>
        </View>
         
        {data && data.data && data.data.map(({ id, title, district, location, min_salary, applicant, state}) => (
          <TouchableRipple 
            key={id}
            onPress={() => navigation.navigate('JawatanKosongFormScreen', {
              jobVacancyId: id,
            })}
          >
            <View key={id}>
              <Divider />
              <List.Item
                title={title}
                description={`${district}, ${location}` + '\n' + state}
                left={props => (
                  <List.Image 
                    variant="image" 
                    source={{
                      uri: 
                        applicant && applicant.logo && applicant.logo.length 
                          ? `${BASE_URL}/${applicant.logo}` 
                          : 'https://ui-avatars.com/api/?name=' + (applicant && applicant.name && applicant.name.replace(' ', '+'))
                    }} 
                  />
                )}
                right={props => <Text variant="titleMedium">RM{min_salary}</Text>}
                style={{ paddingVertical: 0, paddingLeft: 10 }}
              />
              <Divider />
            </View>
          </TouchableRipple>
        ))}
      </ScrollView>
      <FAB
        icon="plus"
        style={styles.fab}
        onPress={() => navigation.navigate('AdminStack', {
          screen: 'JawatanKosongFormScreen'
        })}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

export default JawatanKosongTable;
