import {Dimensions, View, Text, StyleSheet, ScrollView, SafeAreaView} from 'react-native';
import React, { useEffect, useState } from 'react';
import {TextInput, Switch, TouchableRipple, Button, Checkbox, HelperText} from 'react-native-paper';
import DropDown from "react-native-paper-dropdown";
import { Formik } from 'formik';

import { launchImageLibrary } from 'react-native-image-picker';

import { useMalaysiaState, useDistricts, useCities } from '../../../hooks/Api';
import { useUser } from '../../../hooks/PrivateApi';
import moment from 'moment';

const MaklumatProfil = () => {
  const [images, setImages] = useState([]);

  const windowWidth = Dimensions.get('window').width - 40;

  const { data, isLoading, isError } = useUser();

  const [postcode, setPostcode] = useState(data?.applicant?.postcode)

  const malaysianStates = useMalaysiaState(postcode);
  const [statesList, setStatesList] = useState([]);
  const [showStatesListDropdown, setStatesListDropdown] = useState(false);

  const districts = useDistricts(postcode);
  const [districtsList, setDistrictsList] = useState([]);
  const [showDistrictsListDropdown, setShowDistrictsListDropdown] = useState(false);

  const cities = useCities(postcode);
  const [citiesList, setCitiesList] = useState([]);
  const [showCitiesListDropdown, setShowCitiesListDropdown] = useState(false);

  useEffect(() => {
    if (malaysianStates && malaysianStates.data && malaysianStates.data.data) {
      malaysianStates.data.data.map((state) => {
        state.label = state.text;
        state.value = state.id;
      });
    
      setStatesList(malaysianStates.data.data);
    }

    return () => false;
  }, [showStatesListDropdown]);

  useEffect(() => {
    if (districts && districts.data && districts.data.data) {
      districts.data.data.map((district) => {
        district.label = district.text;
        district.value = district.id;
      });
    
      setDistrictsList(districts.data.data);
    }

    return () => false;
  }, [showDistrictsListDropdown]);

  useEffect(() => {
    if (cities && cities.data && cities.data.data) {
      cities.data.data.map((city) => {
        city.label = city.text;
        city.value = city.id;
      });
    
      setCitiesList(cities.data.data);
    }

    return () => false;
  }, [showCitiesListDropdown]);

  return (
    <SafeAreaView
      style={{backgroundColor: '#fff', height: '100%'}}
    >
      <ScrollView style={{ padding: 20 }}>
        <Formik
          enableReinitialize={true}
          initialValues={data}
          onSubmit={async (values) => {}}
        >
          {({ handleChange, handleBlur, handleSubmit, setFieldValue, isSubmitting, values }) => (
            <>
              <TextInput 
                label="Pakej" 
                style={styles.textInput} mode="outlined"
                value={values?.applicant?.package?.name} 
                disabled={true}
              />

              <TextInput 
                label="Tarikh Menyertai" 
                style={styles.textInput} mode="outlined" 
                value={moment(values?.applicant?.created_at).format('DD/MM/YYYY')}
                disabled={true}
              />

              <TextInput 
                label="Tarikh Tamat Tempoh" 
                style={styles.textInput} mode="outlined" 
                value={moment(values?.applicant?.subscription_expiry_date).format('DD/MM/YYYY')}
                disabled={true}
              />

{values?.applicant?.registration_type_id == 1
              ?
                <>
                  <TextInput
                    label="Nama Syarikat"
                    style={styles.textInput}
                    mode="outlined"
                    value={values?.applicant?.company_name}
                    disabled={true}
                  />
                  <HelperText style={{ marginBottom: 10 }}>
                    tidak boleh dikemaskini
                  </HelperText>

                  <TextInput
                    label="No. SSM"
                    style={styles.textInput}
                    mode="outlined"
                    value={values?.applicant?.ssm_no}
                    disabled={true}
                  />
                  <HelperText style={{ marginBottom: 10 }}>
                    tidak boleh dikemaskini
                  </HelperText>
                  
                  {/* <TextInput
                    label="Orang Untuk Dihubungi"
                    style={styles.textInput}
                    mode="outlined"
                    onChangeText={handleChange('contact_person')}
                    onBlur={handleBlur('contact_person')}
                    value={values?.applicant?.contact_person}
                  />
                
                  <TextInput
                    label="No. Telefon Orang Untuk Dihubungi"
                    style={styles.textInput}
                    mode="outlined"
                    onChangeText={handleChange('contact_person_telephone')}
                    onBlur={handleBlur('contact_person_telephone')}
                    value={values?.applicant?.contact_person_telephone}
                  /> */}
                </>
              : null}
              
              <TextInput
                label="Nama"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('applicant.name')}
                onBlur={handleBlur('applicant.name')}
                value={values?.applicant?.name}
                returnKeyType="next"
              />
              
              <TextInput
                label="No MyKad"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('applicant.id_no')}
                onBlur={handleBlur('applicant.id_no')}
                value={values?.applicant?.id_no}
                returnKeyType="next"
              />

              <TextInput
                label="No. Telefon"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('applicant.telephone')}
                onBlur={handleBlur('applicant.telephone')}
                value={values?.applicant?.telephone}
                returnKeyType="next"
              />

              <View style={{ flexDirection: 'row' }}>
                <Checkbox 
                  status={values?.applicant?.show_phone_no ? 'checked' : 'unchecked'}
                  onPress={() => setFieldValue('applicant.show_phone_no', !values?.applicant?.show_phone_no)}
                />
                <Text 
                  style={{ paddingTop: 8 }}
                  onPress={() => setFieldValue('applicant.show_phone_no', !values?.applicant?.show_phone_no)}
                >
                  Papar no. telefon di laman web?
                </Text>
              </View>

              <TextInput
                label="No. WhatsApp"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('applicant.whatsapp_no')}
                onBlur={handleBlur('applicant.whatsapp_no')}
                value={values?.applicant?.whatsapp_no}
                returnKeyType="next"
              />

              <TextInput
                label="E-mel"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('applicant.email')}
                onBlur={handleBlur('applicant.email')}
                value={values?.applicant?.email}
              />

              {values?.applicant?.registration_type_id == 1
              ?
                <>
                  {/* <TextInput
                    label="Nama Syarikat"
                    style={styles.textInput}
                    mode="outlined"
                    value={values?.applicant?.company_name}
                    disabled={true}
                  />
                  <HelperText style={{ marginBottom: 10 }}>
                    tidak boleh dikemaskini
                  </HelperText>

                  <TextInput
                    label="No. SSM"
                    style={styles.textInput}
                    mode="outlined"
                    value={values?.applicant?.ssm_no}
                    disabled={true}
                  />
                  <HelperText style={{ marginBottom: 10 }}>
                    tidak boleh dikemaskini
                  </HelperText> */}
                  
                  <TextInput
                    label="Orang Untuk Dihubungi"
                    style={styles.textInput}
                    mode="outlined"
                    onChangeText={handleChange('applicant.contact_person')}
                    onBlur={handleBlur('applicant.contact_person')}
                    value={values?.applicant?.contact_person}
                  />
                
                  <TextInput
                    label="No. Telefon Orang Untuk Dihubungi"
                    style={styles.textInput}
                    mode="outlined"
                    onChangeText={handleChange('applicant.contact_person_telephone')}
                    onBlur={handleBlur('applicant.contact_person_telephone')}
                    value={values?.applicant?.contact_person_telephone}
                  />
                </>
              : null}

              <TextInput
                label="https://www.facebook.com/"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('applicant.facebook')}
                onBlur={handleBlur('applicant.facebook')}
                value={values?.applicant?.facebook}
              />

              <TextInput
                label="https://www.instagram.com/"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('applicant.instagram')}
                onBlur={handleBlur('applicant.instagram')}
                value={values?.applicant?.instagram}
              />

              <TextInput
                label="Laman Web"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('applicant.website')}
                onBlur={handleBlur('applicant.website')}
                value={values?.applicant?.website}
              />
              <HelperText style={{ marginBottom: 10 }}>
                Termasuk http:// atau https://
              </HelperText>

              <TextInput
                label="Penerangan"
                style={styles.textInput}
                mode="outlined"
                multiline={true}
                numberOfLines={5}
                onChangeText={handleChange('applicant.description')}
                onBlur={handleBlur('applicant.description')}
                value={values?.applicant?.description}
                maxLength={3000}
              />
              <HelperText style={{ marginBottom: 10 }}>Maksimum 3000 aksara</HelperText>

              <TextInput
                label="Alamat"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('applicant.address')}
                onBlur={handleBlur('applicant.address')}
                value={values?.applicant?.address}
              />

              <TextInput
                label="Poskod"
                style={styles.textInput}
                mode="outlined"
                onChangeText={(value) => {
                  setFieldValue('applicant.postcode', value);

                  setPostcode(value);
                }}
                onBlur={handleBlur('applicant.postcode')}
                value={values?.applicant?.postcode}
              />

              <DropDown
                label={"Negeri"}
                mode={"outlined"}
                visible={showStatesListDropdown}
                showDropDown={() => setStatesListDropdown(true)}
                onDismiss={() => setStatesListDropdown(false)}
                value={values?.applicant?.state_code}
                setValue={(value) => setFieldValue('applicant.state_code', value)}
                list={statesList}
              />

              <DropDown
                label={"Daerah"}
                mode={"outlined"}
                visible={showDistrictsListDropdown}
                showDropDown={() => setShowDistrictsListDropdown(true)}
                onDismiss={() => setShowDistrictsListDropdown(false)}
                value={values?.applicant?.district}
                setValue={(value) => setFieldValue('applicant.district', value)}
                list={districtsList}
              />

              <DropDown
                label={"Bandar"}
                mode={"outlined"}
                visible={showCitiesListDropdown}
                showDropDown={() => setShowCitiesListDropdown(true)}
                onDismiss={() => setShowCitiesListDropdown(false)}
                value={values?.applicant?.city}
                setValue={(value) => setFieldValue('applicant.city', value)}
                list={citiesList}
              />

              <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                <Checkbox 
                  status={values?.applicant?.agent ? 'checked' : 'unchecked'}
                  onPress={() => setFieldValue('applicant.agent', !values?.applicant?.agent)}
                />
                <Text 
                  style={{ paddingTop: 8, marginRight: 40 }}
                  onPress={() => setFieldValue('applicant.agent', !values?.applicant?.agent)}
                >
                  Adakah anda bersetuju menjadi ejen produk usahawan lain?
                </Text>
              </View>

              <Button 
                mode="contained" 
                style={{ marginBottom: 40 }}
              >
                Kemaskini
              </Button>
            </>
          )}
        </Formik>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  modal: {
    margin: 5,
  },
  textHeader: {
    margin: 5,
    color: '#000',
  },
  textInput: {
    marginBottom: 5,
  },
});

export default MaklumatProfil;
