import {View, SafeAreaView, ScrollView, Text,} from 'react-native';
import React from 'react';
import {Searchbar, DataTable} from 'react-native-paper';
import {SafeAreaView} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

const numberOfItemsPerPageList = [2, 3, 4];
const items = [
  {
    key: 1,
    name: 'Page 1',
  },
  {
    key: 2,
    name: 'Page 2',
  },
  {
    key: 3,
    name: 'Page 3',
  },
];
const PenambahanPakej = () => {

  const [page, setPage] = React.useState(0);
  const [numberOfItemsPerPage, onItemsPerPageChange] = React.useState(
    numberOfItemsPerPageList[0],
  );
  const from = page * numberOfItemsPerPage;
  const to = Math.min((page + 1) * numberOfItemsPerPage, items.length);

  React.useEffect(() => {
    setPage(0);
  }, [numberOfItemsPerPage]);

  return (
    <SafeAreaView style={{backgroundColor: '#fff'}}>
      <ScrollView>
        <View>
          <Text style={{color: 'red', fontSize: 11, marginTop: 10}}>
            Tiada penambahan pakej buat sementara waktu
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default PenambahanPakej;
