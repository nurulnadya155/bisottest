import {View, Text, SafeAreaView, ScrollView, StyleSheet} from 'react-native';
import React from 'react';
import {TextInput, Button} from 'react-native-paper';

const MaklumatPembayaran = () => {
  return (
    <SafeAreaView
      style={{backgroundColor: '#fff', height: '100%', padding: 20}}>
      <ScrollView>
        <View>
        <Text style={{fontSize: 18, color: '#000'}}> Bayaran</Text>
          <Text style={{fontSize: 15, color: '#000'}}> Maklumat Pembayaran</Text>
        </View>

        <View style={{justifyContent: 'center'}}>
          <TextInput
            label="Nama Pemohon"
            style={styles.textInput}
            mode="outlined"
          />
          <TextInput label="Tarikh" style={styles.textInput} mode="outlined" />
          <TextInput label="Pakej" style={styles.textInput} mode="outlined" />
          <TextInput
            label="Jumlah Bayaran"
            style={styles.textInput}
            mode="outlined"
          />
          <TextInput
            label="Saluran Pembayaran"
            style={styles.textInput}
            mode="outlined"
          />
          <TextInput
            label="ID Transaksi"
            style={styles.textInput}
            mode="outlined"
          />
          <TextInput
            label="Muatnaik"
            style={styles.textInput}
            mode="outlined"
          />
          <TextInput
            label="Kenyataan"
            style={styles.kenyataan}
            mode="outlined"
          />

          <View style={{marginTop: 15, justifyContent: 'space-evenly'}}>
            <Button mode="contained">Hantar</Button>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
    modal: {
      margin: 5,
    },
    textHeader: {
      margin: 5,
      color: '#000',
    },
    textInput: {
      marginBottom: 5,
    },
    kenyataan:{
        marginBottom: 5,
        paddingBottom: 50,
    },
  });

export default MaklumatPembayaran;
