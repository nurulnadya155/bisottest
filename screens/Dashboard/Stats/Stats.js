import {View, StyleSheet, SafeAreaView} from 'react-native';
import React from 'react';

import {Card, List, TouchableRipple, Text} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

import { CountUp } from 'use-count-up';

import { useDashboardStats } from '../../../hooks/PrivateApi';

const Stats = () => {
  const navigation = useNavigation();

  const { data, isLoading, isError } = useDashboardStats();

  return (
    <SafeAreaView style={{height: '100%'}}>
      <View style={{margin: 10, alignItems: 'center'}}>
        <View style={{flexDirection: 'row'}}>
            <Card 
              style={styles.card}
              onPress={() => navigation.navigate('AdminStack', {
                screen: 'ProdukTableScreen',
              })}
            >
              <Card.Content>
                <Text variant="titleLarge">
                  <CountUp isCounting end={data?.products} duration={1} />
                </Text>
                {/* <Text variant="titleLarge">{data?.products ?? '~'}</Text> */}
                <Text variant="bodyMedium">Produk/Perkhidmatan Aktif</Text>
              </Card.Content>
            </Card>

            <Card 
              style={styles.card}
              onPress={() => navigation.navigate('AdminStack', {
                screen: 'AcaraTableScreen'
              })}
            >
              <Card.Content>
                <Text variant="titleLarge">
                  <CountUp isCounting end={data?.events} duration={1} />
                </Text>
                {/* <Text variant="titleLarge">{data?.events ?? '~'}</Text> */}
                <Text variant="bodyMedium">Jumlah Acara Aktif</Text>
              </Card.Content>
            </Card>
        </View>

        <View style={{flexDirection: 'row'}}>
          <Card 
            style={styles.card}
            onPress={() => navigation.navigate('AdminStack', {
              screen: 'JawatanKosongTableScreen'
            })}
          >
            <Card.Content>
              <Text variant="titleLarge">
                <CountUp isCounting end={data?.jobs} duration={1} />
              </Text>
              {/* <Text variant="titleLarge">{data?.jobs ?? '~'}</Text> */}
              <Text variant="bodyMedium">Jumlah Jawatan Kosong Aktif</Text>
            </Card.Content>
          </Card>

          <Card style={styles.card}>
            <Card.Content>
              <Text variant="titleLarge">0</Text>
              <Text variant="bodyMedium">Jumlah Ulasan</Text>
            </Card.Content>
          </Card>
        </View>
      </View>

      <View>
        <TouchableRipple onPress={() => navigation.navigate('AdminStack', {
          screen: 'ProdukTableScreen',
          params: {
            status: 'Pending',
          }
        })}>
          <View style={styles.box1}>
            <Text style={{marginLeft: 20}}>
              Produk/Perkhidmatan Belum Disahkan
            </Text>
            <Icon name="angle-right" size={20} color="black" type="entypo" style={{marginRight: 20}} />
          </View>
        </TouchableRipple>

        <TouchableRipple onPress={() => navigation.navigate('AdminStack', {
          screen: 'AcaraTableScreen',
          params: {
            status: 'Pending',
          }
        })}>
          <View style={styles.box1}>
            <Text style={{marginLeft: 20}}>Acara Yang Belum Disahkan</Text>
            <Icon name="angle-right" size={20} color="black" type="entypo" style={{marginRight: 20}} />
          </View>
        </TouchableRipple>
        
        <TouchableRipple onPress={() => navigation.navigate('AdminStack', {
          screen: 'JawatanKosongTableScreen',
          params: {
            status: 'Pending',
          }
        })}>
          <View style={styles.box1}>
            <Text style={{marginLeft: 20}}>Jawatan Kosong Belum Disahkan</Text>
            <Icon name="angle-right" size={20} color="black" type="entypo" style={{marginRight: 20}} />
          </View>
        </TouchableRipple>

        <View style={styles.box1}>
          <Text style={{marginLeft: 20}}>Invois Terkini</Text>
          <Icon name="angle-right" size={20} color="black" type="entypo" style={{marginRight: 20}} />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  card: {
    margin: 10,
    width: 160,
    alignItems: 'center',
  },
  box1: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 60,
    margin: 10,
    borderRadius: 10,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
    backgroundColor: '#FDF4F3',
  },
});

export default Stats;
