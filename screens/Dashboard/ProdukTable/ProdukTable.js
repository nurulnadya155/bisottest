import {View, StyleSheet, Text} from 'react-native';
import React from 'react';
import {Divider, FAB, Searchbar, List, TouchableRipple} from 'react-native-paper';
import {SafeAreaView} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

import { BASE_URL } from '../../../hooks/Api';
import { useProductIndex } from '../../../hooks/PrivateApi';
import { useNavigation } from '@react-navigation/native';

const ProdukTable = ({ route }) => {
  const status = route?.params?.status;

  const navigation = useNavigation();

  const [searchQuery, setSearchQuery] = React.useState('');

  const onChangeSearch = query => setSearchQuery(query);

  const [page, setPage] = React.useState(0);

  const { data, isLoading, isError } = useProductIndex(searchQuery, status, page);

  return (
    <SafeAreaView style={{backgroundColor:'#fff', height: '100%' }}>
      <ScrollView>
        <Searchbar
        style={{backgroundColor:'#fff'}}
          placeholder="Cari produk"
          onChangeText={onChangeSearch}
          value={searchQuery}
        />
        <View>
          <Text style={{color: 'red', fontSize:11, marginTop:10}}>
            Tiada penambahan pakej buat sementara waktu
          </Text>
        </View>
        
        {data && data.data && data.data.map(({ id, title, applicant, images, price, state }, i) => (
          <TouchableRipple 
            key={id}
            onPress={() => navigation.navigate('ProdukFormScreen', {
              productId: id,
            })}
          >
            <View key={id}>
              <Divider />
              <List.Item
                title={title}
                description={(applicant && applicant.company_name.length ? applicant.company_name : applicant.name) + '\n' + state}
                left={props => <List.Image 
                    variant="image" 
                    source={{
                      uri: images 
                        && images[0] 
                        && images[0].url 
                        ? `${BASE_URL}/${images[0].url}` 
                        : 'https://ui-avatars.com/api/?name=' + title.replace(' ', '+')
                    }}
                  />
                }
                right={props => <Text variant="titleMedium">RM {price}</Text>}
                style={{ paddingVertical: 0, paddingLeft: 10 }}
              />
              <Divider />
            </View>
          </TouchableRipple>
        ))}
      </ScrollView>
      <FAB
        icon="plus"
        style={styles.fab}
        onPress={() => navigation.navigate('AdminStack', {
          screen: 'ProdukFormScreen',
        })}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

export default ProdukTable;
