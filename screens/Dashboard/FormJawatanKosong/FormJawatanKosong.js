import {View, Text, StyleSheet, ScrollView, SafeAreaView} from 'react-native';
import React, { useEffect, useState } from 'react';
import {ActivityIndicator, TextInput, Switch, TouchableRipple, Button, Checkbox, HelperText} from 'react-native-paper';
import { DatePickerModal } from 'react-native-paper-dates';
import Snackbar from 'react-native-snackbar';
import Spinner from 'react-native-loading-spinner-overlay';
import { Formik } from 'formik';
import { PaperSelect } from 'react-native-paper-select';
import moment from 'moment';

import { useLookups, useMalaysiaState, useDistricts } from '../../../hooks/Api';
import { useJobVacancy } from '../../../hooks/PrivateApi';

const FormJawatanKosong = ({ route }) => {
  const jobVacancyId = route?.params?.jobVacancyId;

  const [openCalendar, setOpenCalendar] = useState(false);
  const [openPublicationDateCalendar, setOpenPublicationDateCalendar] = useState(false);

  const [location, setLocation] = useState(data?.location);

  const malaysianStates = useMalaysiaState();
  const [malaysianState, setStateList] = useState({
    value: data?.state_code,
    list: [],
    selectedList: [{ _id: data?.state_code, value: data?.state_code }],
    error: '',
  });

  const districts = useDistricts('', location);
  const [district, setDistrictList] = useState({
    value: data?.district,
    list: [],
    selectedList: [{ _id: data?.district, value: data?.district }],
    error: '',
  });

  const jobCategories = useLookups('job-types');
  const [jobCategory, setJobCategoryList] = useState({
    value: data?.job_type?.name,
    list: [],
    selectedList: [{ _id: data?.job_type?.id, value: data?.job_type?.name }],
    error: '',
  });

  const jobTypes = useLookups('alt-job-types');
  const [jobType, setJobTypeList] = useState({
    value: data?.alt_job_type?.name,
    list: [],
    selectedList: [{ _id: data?.job_type?.id, value: data?.alt_job_type?.name }],
    error: '',
  });

  const industryTypes = useLookups('industry-types');
  const [industryType, setIndustryTypeList] = useState({
    value: data?.industry_type?.name,
    list: [],
    selectedList: [{ _id: data?.industry_type?.id, value: data?.industry_type?.name }],
    error: '',
  });

  const { data, isLoading, isError } = useJobVacancy(jobVacancyId);

  useEffect(() => {
    if (malaysianStates && malaysianStates.data && malaysianStates.data.data) {
      malaysianStates.data.data.map((state) => {
        state.value = state.text;
        state._id   = state.id;
      });

      setStateList({
        ...malaysianState,
        list: malaysianStates.data.data
      });
    }

    return () => false;
  }, [malaysianStates?.data?.data]);

  useEffect(() => {
    if (districts && districts.data && districts.data.data) {
      districts.data.data.map((district) => {
        district.value = district.text;
        district._id   = district.id;
      });

      setDistrictList({
        ...district,
        list: districts.data.data
      });
    }

    return () => false;
  }, [districts?.data?.data]);

  useEffect(() => {
    if (data && data.location) {
      setStateList({
        ...malaysianState,
        value: data.location,
        selectedList: [
          {
            value: data.location,
            _id: data.location,
          }
        ]
      });
    }
  }, [data?.location]);

  useEffect(() => {
    if (data && data.district) {
      setDistrictList({
        ...district,
        value: data.district,
        selectedList: [
          {
            value: data.district,
            _id: data.district,
          }
        ]
      });
    }
  }, [data?.district]);

  useEffect(() => {
    if (jobCategories && jobCategories.data && jobCategories.data.data) {
      jobCategories.data.data.map((jobCategory) => {
        jobCategory.value = jobCategory.name;
        jobCategory._id = jobCategory.id;
      });
    
      setJobCategoryList({
        ...jobCategory,
        list: jobCategories.data.data
      });
    }

    return () => false;
  }, [jobCategories?.data?.data]);

  useEffect(() => {
    if (data && data.job_type) {
      setJobCategoryList({
        ...jobCategory,
        value: data.job_type.name,
        selectedList: [
          {
            value: data.job_type.name,
            _id: data.job_type.id,
          }
        ]
      });
    }
  }, [data?.job_type?.name]);

  useEffect(() => {
    if (jobTypes && jobTypes.data && jobTypes.data.data) {
      jobTypes.data.data.map((jobType) => {
        jobType.value = jobType.name;
        jobType._id = jobType.id;
      });
    
      setJobTypeList({
        ...jobType,
        list: jobTypes.data.data
      });
    }

    return () => false;
  }, [jobTypes?.data?.data]);

  useEffect(() => {
    if (data && data.alt_job_type) {
      setJobTypeList({
        ...jobType,
        value: data.alt_job_type.name,
        selectedList: [
          {
            value: data.alt_job_type.name,
            _id: data.alt_job_type.id,
          }
        ]
      });
    }
  }, [data?.alt_job_type?.name]);

  useEffect(() => {
    if (industryTypes && industryTypes.data && industryTypes.data.data) {
      industryTypes.data.data.map((industryType) => {
        industryType.value = industryType.name;
        industryType._id = industryType.id;
      });
    
      setIndustryTypeList({
        ...industryType,
        list: industryTypes.data.data
      });
    }

    return () => false;
  }, [industryType?.data?.data]);

  if (isLoading) {
    return <ActivityIndicator />
  }

  return (
    <SafeAreaView
      style={{backgroundColor: '#fff', height: '100%'}}
    >
      <ScrollView style={{ padding: 20 }}>
      < Formik
          enableReinitialize={true}
          initialValues={{...data, ...{
            active: false,
            send_for_approval: false,
          }}}
          onSubmit={(values, actions) => {
            setTimeout(() => {
              actions.setSubmitting(false);
            }, 1000);

            setTimeout(() => {
              Snackbar.show({
                text: 'Ralat, simpan data tidak berjaya',
                duration: Snackbar.LENGTH_SHORT,
              });
            }, 1200);
          }}
        >
          {({ handleChange, handleBlur, handleSubmit, setFieldValue, isSubmitting, values }) => (
            <>
              <Spinner
                customIndicator={<ActivityIndicator size="large" />}
                visible={isSubmitting}
                textContent={'Saving...'}
                cancelable={true}
              />

              <TextInput
                label="Jawatan yang Ditawarkan"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('title')}
                onBlur={handleBlur('title')}
                value={values?.title}
              />

              <PaperSelect
                label="Negeri"
                value={malaysianState.value}
                onSelection={(value) => {
                  setStateList({
                    ...malaysianState,
                    value: value.text,
                    selectedList: value.selectedList,
                    error: '',
                  });

                  setDistrictList({
                    ...district,
                    value: '',
                    selectedList: [{ _id: null, value: '' }],
                    error: '',
                  });

                  setFieldValue('location', value.selectedList[0]._id);

                  setLocation(value.selectedList[0]._id);
                }}
                arrayList={[...malaysianState.list]}
                selectedArrayList={[...malaysianState.selectedList]}
                errorText={malaysianState.error}
                multiEnable={false}
              />

              <PaperSelect
                label="Daerah"
                value={district.value}
                onSelection={(value) => {
                  setDistrictList({
                    ...district,
                    value: value.text,
                    selectedList: value.selectedList,
                    error: '',
                  });

                  setFieldValue('district', value.selectedList[0]._id);
                }}
                arrayList={[...district.list]}
                selectedArrayList={[...district.selectedList]}
                errorText={district.error}
                multiEnable={false}
              />

              <DatePickerModal
                // locale="en"
                mode="single"
                visible={openCalendar}
                onDismiss={() => setOpenCalendar(false)}
                // startDate={range.startDate}
                // endDate={range.endDate}
                onConfirm={() => {
                  setOpenCalendar(false);
                }}
              />

              <TextInput
                label="Tarikh Tutup"
                style={styles.textInput}
                mode="outlined"
                value={moment(values?.closing_date).format('DD/MM/Y')}
                onFocus={() => setOpenCalendar(true)}
              />

              <TextInput
                label="Maklumat Pekerjaan Ditawarkan"
                style={styles.textInput}
                mode="outlined"
                multiline={true}
                numberOfLines={5}
                onChangeText={handleChange('description')}
                onBlur={handleBlur('description')}
                value={values?.description}
              />

              <PaperSelect
                label="Kategori Pekerjaan"
                value={jobCategory.value}
                onSelection={(value) => {
                  setJobCategoryList({
                    ...jobCategory,
                    value: value.text,
                    selectedList: value.selectedList,
                    error: '',
                  });

                  setFieldValue('job_type_id', value.selectedList[0]._id);
                }}
                arrayList={[...jobCategory.list]}
                selectedArrayList={[...jobCategory.selectedList]}
                errorText={jobCategory.error}
                multiEnable={false}
              />
              
              <PaperSelect
                label="Jenis Pekerjaan"
                value={jobType.value}
                onSelection={(value) => {
                  setJobTypeList({
                    ...jobType,
                    value: value.text,
                    selectedList: value.selectedList,
                    error: '',
                  });

                  setFieldValue('alt_job_type_id', value.selectedList[0]._id);
                }}
                arrayList={[...jobType.list]}
                selectedArrayList={[...jobType.selectedList]}
                errorText={jobType.error}
                multiEnable={false}
              />

              <PaperSelect
                label="Jenis Industri"
                value={industryType.value}
                onSelection={(value) => {
                  setIndustryTypeList({
                    ...industryType,
                    value: value.text,
                    selectedList: value.selectedList,
                    error: '',
                  });

                  setFieldValue('industry_type_id', value.selectedList[0]._id);
                }}
                arrayList={[...industryType.list]}
                selectedArrayList={[...industryType.selectedList]}
                errorText={industryType.error}
                multiEnable={false}
              />

              <TextInput
                label="Gaji Min."
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('min_salary')}
                onBlur={handleBlur('min_salary')}
                value={values?.min_salary}
              />

              <TextInput
                label="Gaji Maks."
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('max_salary')}
                onBlur={handleBlur('max_salary')}
                value={values?.max_salary}
              />

              <TextInput
                label="Pengalaman Diperlukan"
                style={styles.textInput}
                mode="outlined"
                onChangeText={handleChange('min_experience')}
                onBlur={handleBlur('min_experience')}
                value={values?.min_experience}
              />

              <DatePickerModal
                // locale="en"
                mode="single"
                visible={openPublicationDateCalendar}
                onDismiss={() => setOpenPublicationDateCalendar(false)}
                // startDate={range.startDate}
                // endDate={range.endDate}
                onConfirm={() => {
                  setOpenPublicationDateCalendar(false);
                }}
              />

              <TextInput
                label="Tarikh Publikasi"
                style={styles.textInput}
                mode="outlined"
                value={moment(values?.publish_date).format('DD/MM/Y')}
                onFocus={() => setOpenPublicationDateCalendar(true)}
              />

              <View style={{ flexDirection: 'row' }}>
              <Checkbox 
                  status={values?.active ? 'checked' : 'unchecked'}
                  onPress={() => setFieldValue('active', !values?.active)}
                />
                <Text 
                  style={{ paddingTop: 8 }}
                  onPress={() => setFieldValue('active', !values?.active)}
                >
                  Aktif
                </Text>
              </View>

              <View style={{ flexDirection: 'row' }}>
                <Checkbox 
                  status={values?.send_for_approval ? 'checked' : 'unchecked'}
                  onPress={() => setFieldValue('send_for_approval', !values?.send_for_approval)}
                />
                <Text 
                  style={{ paddingTop: 8 }}
                  onPress={() => setFieldValue('send_for_approval', !values.send_for_approval)}
                >
                  Hantar Untuk Kelulusan
                </Text>
              </View>
              <HelperText style={{ marginBottom: 20 }}>
                jawatan kosong tidak boleh diubah suai selepas permohonan diluluskan oleh admin
              </HelperText>

              <Button 
                mode="contained" 
                style={{ marginBottom: 40 }}
                disabled={values?.state == 'Approved' || isSubmitting}
                onPress={handleSubmit}
              >
                {data?.id ? 'Kemaskini' : 'Hantar'}
              </Button>
            </>
          )}
        </Formik>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  modal: {
    margin: 5,
  },
  textHeader: {
    margin: 5,
    color: '#000',
  },
  textInput: {
    marginBottom: 5,
  },
});

export default FormJawatanKosong;
