import {View, StyleSheet, Text} from 'react-native';
import React from 'react';
import {Divider, FAB, List, Searchbar, TouchableRipple} from 'react-native-paper';
import {SafeAreaView} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

import { BASE_URL } from '../../../hooks/Api';
import { useEventIndex } from '../../../hooks/PrivateApi';

import moment from 'moment';
import { useNavigation } from '@react-navigation/native';

const AcaraTable = ({ route }) => {
  const status = route?.params?.status;

  const navigation = useNavigation();

  const [searchQuery, setSearchQuery] = React.useState('');

  const onChangeSearch = query => setSearchQuery(query);

  const [page, setPage] = React.useState(0);

  const { data, isLoading, isError } = useEventIndex(searchQuery, status, page);

  return (
    <SafeAreaView style={{backgroundColor:'#fff', height: '100%'}}>
      <ScrollView>
        <Searchbar
        style={{backgroundColor:'#fff'}}
          placeholder="Cari acara"
          onChangeText={onChangeSearch}
          value={searchQuery}
        />
        <View>
          <Text style={{color: 'red', fontSize:11, marginTop:10}}>
            Tiada penambahan pakej buat sementara waktu
          </Text>
        </View>
        {data &&
          data.data &&
          data.data.map(
            ({
              id,
              name,
              banner_image,
              city,
              state_code,
              start_date,
              end_date,
              state,
            }) => (
              <TouchableRipple 
                key={id}
                onPress={() => navigation.navigate('AcaraFormScreen', {
                  eventId: id,
                })}
              >
                <View key={id}>
                  <Divider />
                  <List.Item
                    title={name}
                    description={`${city}, ${state_code}` + '\n' + state}
                    left={props => (
                      <List.Image
                        variant="image"
                        source={{
                          uri:
                            banner_image && banner_image.length
                              ? `${BASE_URL}/${banner_image}`
                              : 'https://ui-avatars.com/api/?name=' + name.replace(' ', '+'),
                        }}
                      />
                    )}
                    right={props => <Text variant="titleMedium">{moment(start_date).format(`d MMM YYYY`)}</Text>}
                    style={{ paddingVertical: 0, paddingLeft: 10 }}
                  />
                  <Divider />
                </View>
              </TouchableRipple>
            ),
          )}
      </ScrollView>
      <FAB
        icon="plus"
        style={styles.fab}
        onPress={() => navigation.navigate('AdminStack', {
          screen: 'AcaraFormScreen'
        })}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

export default AcaraTable;
