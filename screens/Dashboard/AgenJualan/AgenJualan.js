import {View, Text} from 'react-native';
import React from 'react';
import { DataTable} from 'react-native-paper';
import {SafeAreaView} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

const numberOfItemsPerPageList = [2, 3, 4];
const items = [
  {
    key: 1,
    name: 'Page 1',
  },
  {
    key: 2,
    name: 'Page 2',
  },
  {
    key: 3,
    name: 'Page 3',
  },
];

const AgenJualan = () => {

  const [page, setPage] = React.useState(0);
  const [numberOfItemsPerPage, onItemsPerPageChange] = React.useState(
    numberOfItemsPerPageList[0],
  );
  const from = page * numberOfItemsPerPage;
  const to = Math.min((page + 1) * numberOfItemsPerPage, items.length);

  React.useEffect(() => {
    setPage(0);
  }, [numberOfItemsPerPage]);

  return (
    <SafeAreaView style={{backgroundColor:'#fff', height:'100%'}}>
      <ScrollView>
        <View>
        <Text style={{color: 'red', fontSize:11, marginTop:10}}>
          Tiada penambahan pakej buat sementara waktu
        </Text>
        </View>
        <DataTable style={{marginTop: 20}}>
          <DataTable.Header>
            <DataTable.Title>ID</DataTable.Title>
            <DataTable.Title>Nama</DataTable.Title>
            <DataTable.Title>E-mel</DataTable.Title>
            <DataTable.Title>Telefon</DataTable.Title>
            <DataTable.Title>Daerah</DataTable.Title>
            <DataTable.Title>Status</DataTable.Title>
          </DataTable.Header>

          <DataTable.Row></DataTable.Row>

          <DataTable.Pagination
            page={page}
            numberOfPages={Math.ceil(items.length / numberOfItemsPerPage)}
            onPageChange={page => setPage(page)}
            label={`${from + 1}-${to} of ${items.length}`}
            showFastPaginationControls
            numberOfItemsPerPageList={numberOfItemsPerPageList}
            numberOfItemsPerPage={numberOfItemsPerPage}
            onItemsPerPageChange={onItemsPerPageChange}
            selectPageDropdownLabel={'Rows per page'}
          />
        </DataTable>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AgenJualan;
