import {View, StyleSheet, ScrollView, Image, TouchableOpacity} from 'react-native';
import { ActivityIndicator, Card, Text } from 'react-native-paper';
import React, { useEffect } from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { SvgUri } from 'react-native-svg';
import { useGetPublicCategories } from '../../hooks/Api';
import { useNavigation } from '@react-navigation/native';
import SenaraiProduk from '../Produk/SenaraiProduk';

const IconLain = () => {
  const navigation = useNavigation();

  const { data, isLoading, isError } = useGetPublicCategories();

  useEffect(() => {
    // 
  }, []);

  if (!data && isLoading) return <ActivityIndicator />;

  return (
    <SafeAreaView>
      <Text style={styles.usahawanTxt}> Kategori Umum</Text>
      <View style={styles.subView}>
        {data && data.map(({ id, name, attributes, slug, count }) => (
          <View key={id} style={styles.container}>
            <Card
              onPress={() => navigation.navigate('Produk', {
                screen: 'SenaraiProduk',
                params: {
                  category: slug,
                }
              })}
            >
              <Card.Content 
                style={{ 
                  alignContent: 'center', 
                  alignItems: 'center', 
                  width: 90, 
                  height: 90,
                  padding: 0
                }}
              >
              <SvgUri
                  width="20"
                  height="20"
                  uri={'https://bisot.com.my/themes/pinlist/images/' + attributes['icon-pinlist']}
                  fill="blue"
                />
                <Text style={styles.txt}>
                  {name} ({count ?? 0})
                </Text>
              </Card.Content>
            </Card>
          </View>
        ))}

        {/* {data && data.map(({ id, name, attributes, slug }) => (
          <TouchableOpacity 
            key={id}
            onPress={() => navigation.navigate('Produk', {
              screen: 'SenaraiProduk',
              params: {
                category: slug,
              }
            })}
          >
            <View style={styles.container}>
              <SvgUri
                width="20"
                height="20"
                uri={'https://bisot.com.my/themes/pinlist/images/' + attributes['icon-pinlist']}
              />
              <Text style={styles.txt}>{name}</Text>
            </View>
          </TouchableOpacity>
        ))} */}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 5,
    // width: 90,
    // height: 90,
    alignItems: 'center',
    // paddingVertical: 8,
    // backgroundColor: '#e8ebf3',
  },
  subView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between', 
    margin: 5
  },
  usahawanTxt: {
    margin: 5,
    marginTop: 30,
    fontSize: 15,
    color: '#0b5394',
  },
  txt: {
    fontSize: 10,
    // color: '#231a14',
    textAlign: 'center',
  },
  tinyLogo: {
    width: 20,
    height: 20,
  },
});

export default IconLain;
