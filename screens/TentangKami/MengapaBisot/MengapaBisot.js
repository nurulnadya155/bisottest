import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import React from 'react';


const MengapaBisot = () => {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <Text style={{fontSize: 18, color: '#000'}}>Mengapa BiSOT?</Text>
        <Text style={styles.txt1}>
          BiSOT adalah singkatan "Biar Semua Orang Tahu", laman web dan aplikasi
          membantu Industri Kecil Sederhana (IKS) mengembangkan pemasaran, dan
          juga pusat sokongan secara menyeluruh untuk meningkatkan produktiviti
          perniagaan.
        </Text>
        <Text style={styles.txt1}>
          BiSOT juga memberi peluang kepada orang awam untuk menjadi ejen kepada
          produk produk usahawan dan mengiklankan produk mereka kepada pembeli
          di sekitar kawasan berdekatan.
        </Text>
        <Text style={styles.txt1}>
          Kepelbagai kategori di BiSOT dapat dimanafaatkan oleh orang ramai
          untuk keperluan harian.
        </Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 10,
    backgroundColor: '#E0E0E0',
  },
  txt1: {
    margin: 5,
    fontFamily: 'sans-serif',
    fontSize: 10,
    textAlign: 'left',
    color:'#000',
  },
});

export default MengapaBisot;
