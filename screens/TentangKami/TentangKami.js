import { SafeAreaView } from 'react-native';
import React from 'react';
import { WebView } from 'react-native-webview';
// import MengapaBisot from './MengapaBisot';
// import Pilihan from './Pilihan';

import { BASE_URL } from '../../hooks/Api';

const TentangKami = () => {
  return (
    <SafeAreaView style={{ flex:1 }}>
      <WebView 
        source={{ uri: `${BASE_URL}/app/pages/about-us` }} 
      />
    </SafeAreaView>
  );
};

export default TentangKami;