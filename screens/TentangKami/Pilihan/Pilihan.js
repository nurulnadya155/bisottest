import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import React from 'react';

import Icon from 'react-native-vector-icons/FontAwesome5';

const Pilihan = () => {
  return (
    <SafeAreaView>
      <Text style={{marginTop: 10,fontSize: 18, color: '#000', textAlign: 'center'}}>
        Mengapa Pilih Kami?
      </Text>
      <View>
        <View style={styles.subView}>
          <View style={styles.container}>
            <Icon name="laptop" size={20} color="black"></Icon>
            <Text style={{color: '#231a14'}}>Ramai Ejen Berdaftar</Text>
            <Text style={styles.txt}>
              {' '}
              BiSOT membantu usahawan mencari ejen-ejen yang berdaftar untuk
              meluaskan pasaran perniagaan produk dan perkhidmatan usahawan di
              seluruh negeri
            </Text>
          </View>

          <View style={styles.container}>
            <Icon name="truck-loading" size={20} color="black"></Icon>
            <Text style={{color: '#231a14'}}>Yuran Setahun Berpatutan</Text>
            <Text style={styles.txt}>
              {' '}
              Yuran bayaran tidak membebankan usahawan. Sila rujuk halaman
              Daftar
            </Text>
          </View>

          <View style={styles.container}>
            <Icon name="users" size={20} color="black"></Icon>
            <Text style={{color: '#231a14'}}>Iklankan Produk Di Pelbagai Kategori</Text>
            <Text style={styles.txt}>
              {' '}
              Usahawan boleh memaparkan produk/perkhidmatan yang sama di
              pelbagai kategori. Ianya menaikan kebarangkalian pembelian
            </Text>
          </View>

          <View style={styles.container}>
            <Icon name="hand-holding" size={20} color="black"></Icon>
            <Text style={{color: '#231a14'}}>Trafik Tinggi</Text>
            <Text style={styles.txt}>
              {' '}
              BiSOT memudahkan orang ramai dengan pencarian "cepat dan tepat"
              kerana menyediakan kategori untuk segala urusan seharian
            </Text>
          </View>

          <View style={styles.container}>
            <Icon name="hand-holding" size={20} color="black"></Icon>
            <Text style={{color: '#231a14'}}>Pakej Tambahan Percuma</Text>
            <Text style={styles.txt}>
              {' '}
              Usahawan diberi pakej mengiklankan jawatan kosong dan acara secara
              percuma
            </Text>
          </View>

          <View style={styles.container}>
            <Icon name="hand-holding" size={20} color="black"></Icon>
            <Text style={{color: '#231a14'}}>Berkongsi Informasi</Text>
            <Text style={styles.txt}>
              {' '}
              BiSOT berkongsi segala informasi dengan pendaftar seperti, acara
              jualan, bantuan dan geran dari agensi kerajaan
            </Text>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  subView: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  txt: {
    margin: 5,
    fontSize: 10,
    textAlign: 'center',
    fontFamily: 'sans-serif',
    color: '#000',
  },
  container: {
    margin: 5,
    width: 350,
    height: 100,
    borderRadius: 5,
    borderColor: 'red',
    alignItems: 'center',
    paddingVertical: 8,
    backgroundColor: '#EEEEEE',
  },
});

export default Pilihan;
