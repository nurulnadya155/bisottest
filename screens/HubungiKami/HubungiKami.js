import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import React, {useState} from 'react';

import {Button, TextInput} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';

import CustomInput from '../../components/CustomInput/CustomInput';

const HubungiKami = () => {
  const [text, onChangeText] = React.useState('');

  return (
    <SafeAreaView style={{backgroundColor:'#fff', height: '100%'}}>
      <Text style={styles.mainTxt}> Hubungi Kami</Text>
      <Text style={styles.mainTxt2}>
        Hubungi kami untuk pertanyaan lanjut atau bantuan.
      </Text>
      <View>
        <View style={styles.subView}>
          <View style={styles.container}>
            <Icon name="map-marker" size={20} color="red"></Icon>
            <Text style={styles.txt}>
              {' '}
              Level 6, Wisma Central, Jalan Ampang
            </Text>
          </View>

          <View style={styles.container}>
            <Icon name="phone" size={20} color="red"></Icon>
            <Text style={styles.txt}> +60-123456789</Text>
          </View>

          <View style={styles.container}>
            <Feather name="mail" size={20} color="red"></Feather>
            <Text style={styles.txt}> admin@bisot.com.my</Text>
          </View>
        </View>
        <View style={styles.inputContainer}>
          <View>
            <TextInput
              style={styles.input}
              onChangeText={onChangeText}
              value={text}
              placeholder="Nama"
            />
            <TextInput
              style={styles.input}
              onChangeText={onChangeText}
              value={text}
              placeholder="E-mel"
            />
            <TextInput
              style={styles.input1}
              onChangeText={onChangeText}
              value={text}
              placeholder="Mesej"
            />
          </View>
          <View>
            <Button
              style={styles.button}
              mode="contained"
              onPress={() => console.log('Pressed')}>
              Hantar
            </Button>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainTxt: {
    marginTop: 30,
    fontSize: 16,
    color: '#000',
    textAlign: 'center',
  },
  mainTxt2: {
    marginTop: 5,
    fontSize: 10,
    textAlign: 'center',
  },
  container: {
    margin: 5,
    width: 88,
    height: 100,
    alignItems: 'center',
  },
  subView: {
    marginLeft: 50,
    margin: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  txt: {
    margin: 10,
    fontSize: 10,
    color: '#231a14',
    textAlign: 'center',
  },
  input: {
    height: 15,
    width: 300,
    margin: 5,
    padding: 8,
    backgroundColor: '#fff',
    fontSize: 11,
    textAlign: 'center',
  },
  input1: {
    height: 80,
    width: 300,
    margin: 5,
    backgroundColor: '#fff',
    fontSize: 11,
    textAlign: 'center',
  },
  inputContainer: {
    margin: 40,
    width: 400,
  },
  button: {
    margin: 50,
    backgroundColor: '#2986cc',
    width: 200,
  },
});

export default HubungiKami;
