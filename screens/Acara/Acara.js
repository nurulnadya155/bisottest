import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import React from 'react';

import Kategori from './Kategori';
import SenaraiAcara from './SenaraiAcara';
import ViewAcara from './ViewAcara';
import IklanPremium from '../IklanPremium';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
const Stack = createNativeStackNavigator();

const Main = () => {
  return (
    <SafeAreaView>
      <Text style={styles.mainTxt}> Daftar Iklan Acara Anda Di Sini</Text>
      <IklanPremium />
      <Kategori />
    </SafeAreaView>
  )
}

const Acara = () => {
  return (
    <Stack.Navigator initialRouteName="Main">
      <Stack.Screen name="Main" component={Main} options={{ headerShown:false }} />
      <Stack.Screen name="SenaraiAcara" component={SenaraiAcara} options={{ title: 'Acara' }} />
      <Stack.Screen name="ViewAcara" component={ViewAcara} options={{ title: 'Acara' }} />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  mainTxt: {
    margin: 10,
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
  },
});

export default Acara;
