import {
  View,
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import {
  Banner, 
  Divider, 
  Text, 
  List,
} from 'react-native-paper';
import React, { useState } from 'react';
import { BASE_URL, useEventIndex } from '../../../hooks/Api';

import moment from 'moment';

const SenaraiAcara = ({ route, navigation }) => {
  const type = route.params && route.params.type ? route.params.type : '';

  const [pageIndex, setPageIndex] = useState(0);

  // const [refreshing, setRefreshing] = React.useState(false);

  const { data, isLoading, isError } = useEventIndex(type, pageIndex);

  const onRefresh = React.useCallback(() => {
    // setRefreshing(true);

    // setTimeout(() => {
    //   setRefreshing(false);
    // }, 2000);
  }, []);

  return (
    <SafeAreaView>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={onRefresh} />
        }>
        {/* <Text style={styles.mainTxt}>Acara</Text> */}

        <Banner
          visible={!data || data && data.data && data.data.length == 0}
        >
            Tiada rekod
        </Banner>

        {data &&
          data.data &&
          data.data.map(
            ({
              id,
              name,
              banner_image,
              city,
              state_code,
              start_date,
              end_date,
            }) => (
              <TouchableOpacity 
                key={id}
                onPress={() => navigation.navigate('ViewAcara', {
                  eventId: id,
                })}
              >
                <View key={id}>
                  <Divider />
                  <List.Item
                    title={name}
                    description={`${city}, ${state_code}`}
                    left={props => (
                      <List.Image
                        variant="image"
                        source={{
                          uri:
                            banner_image && banner_image.length
                              ? `${BASE_URL}/${banner_image}`
                              : 'https://ui-avatars.com/api/?name=' + name.replace(' ', '+'),
                        }}
                      />
                    )}
                    right={props => <Text variant="titleMedium">{moment(start_date).format(`d MMM YYYY`)}</Text>}
                    style={{ paddingVertical: 0, paddingLeft: 10 }}
                  />
                  <Divider />
                </View>
              </TouchableOpacity>
            ),
          )}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainTxt: {
    margin: 10,
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
  },
});

export default SenaraiAcara;
