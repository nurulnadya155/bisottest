import {View, Text, StyleSheet} from 'react-native';
import React from 'react';

import {TextInput, Button} from 'react-native-paper';

const ViewHubungiKami = ({route, navigation}) => {
  const [text, setText] = React.useState('');

  return (
    <View style={styles.modal}>
      <Text style={styles.textHeader}> Hantar Mesej</Text>
      <View>
        <TextInput
          label="Nama Penuh"
          style={styles.textInput}
          mode="outlined"
        />
        <TextInput
          label="E-mel"
          style={styles.textInput}
          mode="outlined"
          returnKeyType="next"
        />
        <TextInput
          label="No Tel"
          style={styles.textInput}
          mode="outlined"
          keyboardType="number-pad"
          returnKeyType="next"
        />
        <TextInput
          label="Kepada"
          style={styles.textInput}
          returnKeyType="next"
          value="value"
          disabled={true}
        />
        <TextInput
          label="Perkara"
          style={styles.textInput}
          mode="outlined"
          returnKeyType="next"
        />
        <TextInput label="Mesej" style={styles.textMesej} mode="outlined" />

        <TextInput
          label="Kod Referal"
          mode="outlined"
          style={styles.txtInput}
          disabled={true}
        />
        <View style={{marginTop: 10, flexDirection:'row', justifyContent:'space-evenly'}}>
          <Button style={styles.buttoncancel} mode="contained">Batal</Button>
          <Button style={styles.buttonsubmit} mode="contained">
            Hantar
          </Button>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  modal: {
    margin: 5,
  },
  textHeader: {
    margin: 5,
    color: '#000',
  },
  textInput: {
    margin: 3,
  },
  textMesej: {
    height: 100,
    margin: 3,
  },
  buttonsubmit: {
    backgroundColor: '#3EDD00',
  },
  buttoncancel: {
    backgroundColor: '#DD4C51',
  }
});

export default ViewHubungiKami;
