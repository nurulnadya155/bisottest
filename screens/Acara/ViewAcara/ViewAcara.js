import {View, SafeAreaView, Image, StyleSheet, ScrollView, RefreshControl} from 'react-native';
import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Divider,
  Text,
  Modal,
  Portal,
  Button,
  Provider,
  Card,
  Chip,
} from 'react-native-paper';
import { SliderBox } from "react-native-image-slider-box";

import Icon from 'react-native-vector-icons/FontAwesome5';

import { BASE_URL, useEvent } from '../../../hooks/Api';

import ViewHubungiKami from '../ViewHubungiKami/ViewHubungiKami';
const ViewAcara = ({ navigation, route }) => {
  // const [visible, setVisible] = React.useState(false);

  // const showModal = () => setVisible(true);
  // const hideModal = () => setVisible(false);
  // const containerStyle = {backgroundColor: 'white', padding: 20};

  const eventId = route?.params?.eventId;

  const { data, isLoading, isError } = useEvent(eventId);

  const [ images, setImages ] = useState([]);

  useEffect(() => {
    // setImages([]);

    // if (data && data.title) {
    //   navigation.setOptions({ title: data.title });
    // }

    var _images = [];

    if (data && data.banner_image) {
      _images.push(BASE_URL + '/' + data.banner_image);

      setImages(_images);
    }
  }, [eventId, isLoading, data]);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={isLoading} />
        }
      >
        <SliderBox 
          autoplay={true}
          images={images}
          resizeMethod="resize"
          resizeMode="cover"
          ImageLoader={<ActivityIndicator />}
          LoaderComponent={props => <ActivityIndicator />}
        />

        <View style={{ padding: 20 }}>
          <Card style={{ marginBottom: 20 }}>
            <Card.Title title={data?.name} titleNumberOfLines={5} />
            <Card.Content>
              
            </Card.Content>
          </Card>

          <Card style={{ marginBottom: 20 }}>
            <Card.Title title="Maklumat Untuk Dihubungi" />
          </Card>

          <Card>
            <Card.Title title="Lokasi Peta" />
          </Card>
        </View>
        {/* <View style={styles.imageContainer}>
          <Image
            source={require('../../../assets/images/jusdurianbelanda.jpg')}
            style={{resizeMode: 'contain'}}
          />
        </View>
        <Text style={styles.textContainer}>
          International Conference on Education Management and Administration
        </Text>
        <View style={{flexDirection: 'row', margin: 5}}>
          <Icon name="map-pin" />
          <Text style={{fontSize: 11, color: 'black', marginLeft: 5}}>
            {' '}
            Langkawi- Kedah
          </Text>
        </View>
        <View style={{flexDirection: 'row', margin: 5}}>
          <Icon name="calendar" />
          <Text style={{fontSize: 11, color: 'black', marginLeft: 5}}>
            {' '}
            23/01/2023
          </Text>
        </View>
        <View>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
            <View>
              <Text style={{width: 140, textAlign: 'center'}}>Penerangan</Text>
            </View>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text>Dikirim oleh: </Text>
            <Text>Event ID: </Text>
          </View>

          <Text style={styles.textPenerangan}>
            BiSOT adalah singkatan "Biar Semua Orang Tahu", laman web dan
            aplikasi membantu Industri Kecil Sederhana (IKS) mengembangkan
            pemasaran, dan juga pusat sokongan secara menyeluruh untuk
            meningkatkan produktiviti perniagaan. BiSOT adalah singkatan "Biar
            Semua Orang Tahu", laman web dan aplikasi membantu Industri Kecil
            Sederhana (IKS) mengembangkan pemasaran, dan juga pusat sokongan
          </Text>
         
        </View>

        <View style={{marginTop: 15}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
            <View>
              <Text style={{width: 200, textAlign: 'center'}}>
                Maklumat Untuk Dihubungi
              </Text>
            </View>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          </View>
          <View>
            <Portal>
              <Modal
                visible={visible}
                onDismiss={hideModal}
                contentContainerStyle={containerStyle}>
                <ViewHubungiKami />
              </Modal>
            </Portal>
            <Button icon="message" textColor="#ea9999" onPress={showModal}>
              Hubungi Kami
            </Button>
          </View>
        </View>

        <View style={{marginTop: 15}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
            <View>
              <Text style={{width: 150, textAlign: 'center'}}>Lokasi Peta</Text>
            </View>
            <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          </View>
        </View> */}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  imageContainer: {
    flex: 0.7,
    alignItems: 'stretch',
  },
  textContainer: {
    color: '#000',
    fontSize: 13,
    fontFamily: 'Sans-Serif',
    marginLeft: 10,
  },
  textPenerangan: {
    color: '#000',
    fontSize: 13,
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default ViewAcara;
