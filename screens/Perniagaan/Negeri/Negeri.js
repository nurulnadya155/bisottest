import {View, StyleSheet, SafeAreaView, TouchableOpacity, Image} from 'react-native';
import { ActivityIndicator, Text } from 'react-native-paper';
import React from 'react';
import { BASE_URL, useBusinessStates } from '../../../hooks/Api';
import { useNavigation } from '@react-navigation/native';

const Negeri = () => {
  const navigation = useNavigation();

  const { data, isLoading, isError } = useBusinessStates();

  if (isLoading) return <ActivityIndicator />;

  return (
    <SafeAreaView>
      <Text style={styles.mainTxt}> Direktori Negeri</Text>
        <View style={styles.subView}>
          {data && data.map(({ code, name, count }) => (
            <TouchableOpacity 
              key={code}
              onPress={() => navigation.navigate('SenaraiPerniagaan', {
                state: code,
              })}
            >
              <View style={styles.container}>
                <Image
                  source={{
                    uri: `${BASE_URL}/images/flags/${code}.png`
                  }}
                  style={{width: 70, height: 70}}
                />
                <Text style={styles.txt}>{name} ({count})</Text>
              </View>
            </TouchableOpacity>
          ))}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 5,
    width: 80,
    height: 80,
    alignItems: 'center',
  },
  subView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  mainTxt: {
    marginTop: 15,
    marginLeft: 5,
    fontSize: 18,
    color: '#0b5394',
  },
  txt: {
    fontSize: 9,
    color: '#000',
    textAlign: 'center',
  },
});

export default Negeri;