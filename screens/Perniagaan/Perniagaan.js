import {View, Text, SafeAreaView, StyleSheet, ScrollView} from 'react-native';
import React from 'react';

import IklanPremium from '../IklanPremium';
import Negeri from './Negeri';
import KategoriPerniagaan from './KategoriPerniagaan';
import SenaraiPerniagaan from './SenaraiPerniagaan';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import SenaraiProduk from '../Produk/SenaraiProduk';
import ViewProduk from '../Produk/ViewProduk';
const Stack = createNativeStackNavigator();

const Main = () => {
  return (
    <SafeAreaView>
      <ScrollView vertical={true} showsVerticalScrollIndicator={false}>
      <Text style={styles.mainTxt}> Cari Direktori Perniagaan Terbaik Anda</Text>
      <IklanPremium />
      <Negeri />
      <KategoriPerniagaan />
      </ScrollView>
      
    </SafeAreaView>
  );
};

const Perniagaan = () => {
  return (
    <Stack.Navigator initialRouteName="Main">
      <Stack.Screen name="Main" component={Main} options={{ headerShown:false }} />
      <Stack.Screen name="SenaraiPerniagaan" component={SenaraiPerniagaan} options={{ title: 'Perniagaan' }} />
      <Stack.Screen name="SenaraiProduk" component={SenaraiProduk} options={{ title: 'Produk' }} />
      <Stack.Screen name="ViewProduk" component={ViewProduk} options={{ title: 'Produk' }} />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  mainTxt: {
    margin: 10,
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
  },
});

export default Perniagaan;
