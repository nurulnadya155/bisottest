import {
  View, 
  RefreshControl, 
  SafeAreaView, 
  StyleSheet, 
  ScrollView
} from 'react-native';
import { 
  ActivityIndicator, 
  Banner,
  Divider, 
  Text, 
  List 
} from 'react-native-paper';
import React, { useState } from 'react';
import { BASE_URL, useBusinessIndex } from '../../../hooks/Api';

const SenaraiPerniagaan = ({ route, navigation }) => {
  const state = route.params && route.params.state ? route.params.state : '';
  const type  = route.params && route.params.type  ? route.params.type  : '';

  const [pageIndex, setPageIndex] = useState(0);

  const [refreshing, setRefreshing] = React.useState(false);

  const { data, isLoading, isError } = useBusinessIndex(state, type, pageIndex);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    setTimeout(() => {
      setRefreshing(false);
    }, 2000);
  }, []);

  return (
    <SafeAreaView>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {/* <Text style={styles.mainTxt}>Perniagaan</Text> */}

        <Banner
          visible={!data || data && data.data && data.data.length == 0}
        >
            Tiada rekod
        </Banner>

        {data && data.data && data.data.map(({ id, name, company_name, logo, registration_type }) => (
          <View key={id}>
            <List.Item
              title={company_name && company_name.length ? company_name : name}
              description={registration_type}
              left={props => <List.Image variant="image" source={{uri: logo && logo.length ? `${BASE_URL}/${logo}` : 'https://ui-avatars.com/api/?name=' + name.replace(' ', '+')}} />}
              style={{ paddingVertical: 0, paddingLeft: 10 }}
              onPress={() => navigation.navigate('Perniagaan', {
                screen: 'SenaraiProduk',
                params: {
                  businessId: id,
                }
              })}
            />
            <Divider />
          </View>
        ))}

      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainTxt: {
    margin: 10,
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
  },
});

export default SenaraiPerniagaan;
