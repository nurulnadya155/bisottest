import { View, StyleSheet, ScrollView, TouchableOpacity} from 'react-native';
import React from 'react';
import { Button, Text} from 'react-native-paper';
import { WebView } from 'react-native-webview';
import {SafeAreaProvider, SafeAreaView} from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';
import { BASE_URL } from '../../hooks/Api';

const Pakej = () => {
  const navigation = useNavigation();

  const script = `document.querySelector('.btn').style.display = 'none';`;

  return (
    <SafeAreaView style={{ flex:1 }}>
      <WebView 
        source={{ uri: `${BASE_URL}/app/pages/packages` }} 
        javaScriptEnabledAndroid={true}
        injectedJavaScript={script}
      />

      <View style={{ margin: 10, alignContent: 'center', alignItems: 'center' }}>
        <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('SignUpScreen')}>
          <Text style={styles.btnTxt}>Daftar</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainTxt: {
    marginTop: 20,
    fontSize: 15,
    textAlign: 'center',
    color: 'red',
  },
  container: {
    flexDirection: 'column',
    margin: 10,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#dcdcdc',
    padding: 10,
    paddingBottom: 20,
    height: 230,
    backgroundColor: '#00e682',
  },
  container2: {
    margin: 10,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#dcdcdc',
    padding: 10,
    paddingBottom: 20,
    height: 300,
    backgroundColor: '#4049ec',
  },
  pakejTxt: {
    margin: 20,
    fontSize: 15,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  btn: {
    height: 30,
    width: 100,
    backgroundColor: '#ff2b88',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnTxt: {
    color: '#fff',
    fontSize: 13,
  },
});

export default Pakej;
