import React from 'react';

import { BASE_URL } from '../hooks/Api';

const AuthContext = React.createContext({
  state: null,
});

export default AuthContext;